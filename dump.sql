-- --------------------------------------------------------
-- Хост:                         192.168.10.10
-- Версия сервера:               5.7.17-0ubuntu0.16.04.2 - (Ubuntu)
-- Операционная система:         Linux
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица accountant.activations
CREATE TABLE IF NOT EXISTS `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.activations: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
	(1, 1, 'yNTeSD02VfXXWGQgAxsA18vkFJQA7OB4', 1, '2017-06-19 14:22:13', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(2, 2, 'UmSZNN59hS8zYEVADP99TeHATNgGWsYU', 1, '2017-06-19 14:22:13', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(3, 3, '0gSh8mMr8rAu7Q20uSi1zwaZBMrG1rJf', 1, '2017-06-19 14:22:13', '2017-06-19 14:22:13', '2017-06-19 14:22:13');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.clients
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passport` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiknikuim` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.clients: ~25 rows (приблизительно)
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` (`id`, `user_id`, `number`, `name`, `passport`, `tiknikuim`, `phone`, `email`, `created_at`, `updated_at`) VALUES
	(1, 3, '4', 'פאדווא', '2964763', '682557', '04-1760430', 'yygs.kspyt@qwq.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(2, 3, '34', 'יונגרייז', '345373', '5034', '972-0-46502614', 'gwrryh.trh@gwrryh.biz', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(3, 3, '51', 'אויערבך', '720303', '9622', '972-5-13963318', 'sbty56@gmail.com', '2017-06-19 14:22:13', '2017-07-18 07:49:29'),
	(4, 3, '815381', 'אייבשיץ', '4', '902', '09-3056167', 'ask@rwqh.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(5, 3, '1467091', 'אלטשולר', '3864460', '417305036', '972-59-7804873', 'shq45@yahoo.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(6, 3, '27098322', 'ברדוגו', '69779005', '400212251', '972-1-04791072', 'ywngryyz.lylk@myrls.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(7, 3, '8', 'ליפשיץ', '90967', '928', '059-7528244', 'jdysqyn@yahoo.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(8, 3, '39', 'ברנדיס', '15608', '1241', '972-58-4566893', 'n.brwd@hotmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(9, 3, '1', 'ויסבלום', '89232', '962', '972-50-2248461', 'wwltr.myyzlys@hwrwbyz.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(10, 3, '16', 'לוברבוים', '7', '9', '972-2-85857438', 'wrynh.trwp@hotmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(11, 3, '449580689', 'ברזובסקי', '35168297', '967884486', '00-6982150', 'bytr.grwsbrd@gmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(12, 3, '589854253', 'איגר', '331', '299927', '0291647906', 'dwn.rylh@hotmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(13, 3, '9313498', 'איסרליש', '376699757', '491823', '0674161164', 'sknzy.lyly@gmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(14, 3, '980266', 'איגר', '74669', '5741383', '972-8-18031634', 'wwlp54@hotmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(15, 3, '19656', 'ליפשיץ', '599', '810776714', '972-52-4695170', 'qrn.lypsyz@hotmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(16, 3, '134686', 'רוטשילד', '6667361', '330835', '050-0044947', 'arpwpwrt@spryn.info', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(17, 3, '10611765', 'קרליבך', '48977446', '7', '07-7454897', 'd.spyr@yahoo.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(18, 3, '617722387', 'טרופ', '53719', '248803', '0845859989', 'yyznstt.rhmym@swrwzqyn.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(19, 3, '4421911', 'מירלש', '984', '83356694', '05-1280988', 'trsysh02@gynzbwrg.info', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(20, 3, '50', 'מישקובסקי', '67', '728344', '00-6422107', 'ispryn@qlypry.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(21, 3, '973', 'מישקובסקי', '28', '3', '972-56-0472707', 'ngh.ydlstyyn@hotmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(22, 3, '335522573', 'הלפרין', '868', '3699876', '00-1292883', 'myrh22@tyngr.info', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(23, 3, '65787527', 'ברנדסדורפר', '15563', '8515', '972-8-61736876', 'spryn.dnyh@gmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(24, 3, '608322996', 'דון', '48653', '91', '03-7927953', 'gwrryh.sny@dysqyn.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(25, 3, '2578', 'הלברשטם', '93340', '9693037', '054-0701811', 'srn.ywsy@gmail.com', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(26, 2, '123', '321', '123', '321', '123', '321@123.com', '2017-06-21 11:04:37', '2017-06-21 11:04:37');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.comments: ~39 rows (приблизительно)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`id`, `user_id`, `parent_id`, `comment`, `created_at`, `updated_at`) VALUES
	(1, 3, 14, '11111111', '2017-06-21 05:55:11', '2017-06-21 05:55:11'),
	(2, 3, 14, '222222222', '2017-06-21 05:55:18', '2017-06-21 05:55:18'),
	(3, 3, 14, 'Pogvorit\' s Denisom pro Accaountant', '2017-06-21 06:03:06', '2017-06-21 06:03:06'),
	(4, 3, 14, 'Proverit\'\' modsecurity na servere', '2017-06-21 06:03:40', '2017-06-21 06:03:40'),
	(5, 3, 18, '123', '2017-06-21 06:04:37', '2017-06-21 06:04:37'),
	(6, 3, 18, '123123123', '2017-06-21 06:04:44', '2017-06-21 06:04:44'),
	(7, 3, 20, 'qwe', '2017-06-21 09:55:51', '2017-06-21 09:55:51'),
	(8, 3, 21, 'asdf', '2017-06-21 10:02:38', '2017-06-21 10:02:38'),
	(9, 3, 22, '321', '2017-06-21 10:03:00', '2017-06-21 10:03:00'),
	(10, 3, 23, '5', '2017-06-21 10:06:26', '2017-06-21 10:06:26'),
	(11, 3, 27, 'dfgsdfgdfg', '2017-06-21 12:28:29', '2017-06-21 12:28:29'),
	(12, 3, 32, '123123', '2017-06-21 12:35:53', '2017-06-21 12:35:53'),
	(13, 3, 35, '123', '2017-06-21 15:06:33', '2017-06-21 15:06:33'),
	(14, 3, 35, '321', '2017-06-21 15:06:35', '2017-06-21 15:06:35'),
	(15, 3, 35, '123', '2017-06-21 15:06:37', '2017-06-21 15:06:37'),
	(16, 3, 43, '312', '2017-06-21 20:28:08', '2017-06-21 20:28:08'),
	(17, 3, 50, '321', '2017-06-21 20:51:28', '2017-06-21 20:51:28'),
	(18, 3, 68, '3123123', '2017-06-21 22:03:35', '2017-06-21 22:03:35'),
	(19, 3, 78, 'asdasd', '2017-06-21 22:31:39', '2017-06-21 22:31:39'),
	(20, 3, 50, '123', '2017-06-21 22:38:59', '2017-06-21 22:38:59'),
	(21, 3, 81, 'asd', '2017-06-21 22:57:28', '2017-06-21 22:57:28'),
	(22, 3, 83, 'sdfsdf', '2017-06-21 23:09:07', '2017-06-21 23:09:07'),
	(23, 3, 50, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci earum id inventore ipsam mollitia nesciunt odit officia placeat quae quam qui, quis, voluptatum.', '2017-06-22 07:29:06', '2017-06-22 07:29:06'),
	(24, 3, 50, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci earum id inventore ipsam mollitia nesciunt odit officia placeat quae quam qui, quis, voluptatum.', '2017-06-22 07:29:08', '2017-06-22 07:29:08'),
	(25, 3, 50, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci earum id inventore ipsam mollitia nesciunt odit officia placeat quae quam qui, quis, voluptatum.', '2017-06-22 07:29:19', '2017-06-22 07:29:19'),
	(27, 3, 127, 'dsfgsdfgdsfg', '2017-06-29 06:36:39', '2017-06-29 06:36:39'),
	(28, 3, 127, 'vhfghfgh', '2017-06-29 06:36:46', '2017-06-29 06:36:46'),
	(29, 3, 127, 'ghdfgh', '2017-06-29 06:36:48', '2017-06-29 06:36:48'),
	(33, 3, 108, 'asdfasdf', '2017-06-29 06:37:43', '2017-06-29 06:37:43'),
	(34, 3, 108, 'asdfasdf', '2017-06-29 06:37:44', '2017-06-29 06:37:44'),
	(35, 3, 108, 'sdfasdf', '2017-06-29 06:37:46', '2017-06-29 06:37:46'),
	(36, 3, 108, 'asdfasdf', '2017-06-29 06:37:47', '2017-06-29 06:37:47'),
	(37, 3, 197, 'fasdfasdf', '2017-06-29 09:23:33', '2017-06-29 09:23:33'),
	(38, 3, 197, 'asdfasdf', '2017-06-29 09:23:36', '2017-06-29 09:23:36'),
	(39, 3, 243, 'asdfasdf', '2017-07-13 08:42:12', '2017-07-13 08:42:12'),
	(40, 3, 245, 'dfsadf', '2017-07-13 10:30:05', '2017-07-13 10:30:05'),
	(41, 3, 245, 'asfdfsadfdddddddeeee', '2017-07-13 10:30:11', '2017-07-13 10:30:11'),
	(42, 3, 247, 'ertdfgdf', '2017-07-17 07:40:55', '2017-07-17 07:40:55'),
	(47, 3, 247, 'hghfghfdgh', '2017-07-17 07:59:11', '2017-07-17 07:59:11'),
	(48, 3, 265, 'роланло', '2017-07-17 09:06:09', '2017-07-17 09:06:09'),
	(49, 3, 265, 'ролпр', '2017-07-17 09:06:11', '2017-07-17 09:06:11'),
	(50, 3, 266, 'sdfasdf', '2017-07-18 07:42:58', '2017-07-18 07:42:58'),
	(51, 3, 266, 'asdfasdf', '2017-07-18 07:42:59', '2017-07-18 07:42:59');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.custom_reports
CREATE TABLE IF NOT EXISTS `custom_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.custom_reports: ~5 rows (приблизительно)
/*!40000 ALTER TABLE `custom_reports` DISABLE KEYS */;
INSERT INTO `custom_reports` (`id`, `user_id`, `name`, `created_at`, `updated_at`) VALUES
	(11, 3, 'one more custom', '2017-06-21 10:06:08', '2017-06-21 10:06:08'),
	(12, 3, 'hello', '2017-06-22 07:54:43', '2017-06-22 07:54:43'),
	(13, 3, 'sdfsdf', '2017-06-22 14:22:33', '2017-06-22 14:22:33'),
	(14, 3, 'qweqwe', '2017-06-29 09:22:41', '2017-06-29 09:22:41'),
	(15, 3, 'custom', '2017-07-13 08:38:59', '2017-07-13 08:38:59');
/*!40000 ALTER TABLE `custom_reports` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.migrations: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(36, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
	(37, '2017_05_14_152835_add_phone_column_to_users', 1),
	(38, '2017_06_01_133621_create_clients_table', 1),
	(39, '2017_06_01_133643_create_reports_table', 1),
	(40, '2017_06_02_120345_create_comments_table', 1),
	(41, '2017_06_05_111808_add_user_id_to_comments', 1),
	(42, '2017_06_14_105323_add_user_id_to_reports', 1),
	(43, '2017_06_15_080910_create_custom_reports_table', 1),
	(44, '2017_06_15_085230_add_cr_id_to_reports', 1),
	(45, '2017_06_21_083416_drop_column_in_custom_reports', 2),
	(47, '2017_06_21_111433_create_todo_items_table', 3),
	(48, '2017_06_22_101326_change_type_column_comment_on_comments', 4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.persistences
CREATE TABLE IF NOT EXISTS `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.persistences: ~10 rows (приблизительно)
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
	(1, 3, 'quJJ9hpxDIJ2efAex24bH0bBkNZEcLyy', '2017-06-19 14:22:21', '2017-06-19 14:22:21'),
	(4, 3, 'v2J8CrmNeywIPJAWZZgcpJ9UFzkYFxcj', '2017-06-21 11:04:52', '2017-06-21 11:04:52'),
	(5, 3, 'ZDmBpLEuo1qThuKT1bLZ6Xz08huOI5M6', '2017-06-21 19:52:43', '2017-06-21 19:52:43'),
	(10, 3, 'd0EWSpv3jO6888GyaFx3OvbdH6PxUtTA', '2017-06-22 13:49:52', '2017-06-22 13:49:52'),
	(11, 3, 'fMmEUIOpP0CH7IhIA8vLP95r6OYpfUg9', '2017-06-22 20:37:10', '2017-06-22 20:37:10'),
	(12, 3, 'ugSxehHqL3os2matISZOSBOS9vY6NFEE', '2017-06-28 05:39:33', '2017-06-28 05:39:33'),
	(13, 3, 'vJWrXzIYn34YtCSxyl3t6d1kG6d8xdcv', '2017-06-29 06:07:24', '2017-06-29 06:07:24'),
	(16, 3, 'jQHGEZ7bR40S2Y2CHBvJYRSBcwszfxT6', '2017-07-13 08:09:32', '2017-07-13 08:09:32'),
	(20, 3, 'q2WZEbrrNak2r3hbeOZj2SaIADQ0sxOz', '2017-07-17 11:52:23', '2017-07-17 11:52:23'),
	(23, 3, 'SNt3GJSbUo7GdCl3bK7PaJRyRNaLrwJW', '2017-07-25 06:12:45', '2017-07-25 06:12:45');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.reminders
CREATE TABLE IF NOT EXISTS `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.reminders: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
INSERT INTO `reminders` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
	(1, 3, 'vnueiJUwPdHl7c4NNGZCzyzdLK2lu0yV', 1, '2017-06-22 13:16:24', '2017-06-22 13:10:23', '2017-06-22 13:16:24');
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `cr_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settings` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('pending','draft','complete') COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('annual-report','declaration-of-capital','ishur-nikui-mas-bamakor','mam','mas-ahnasa','bituach-leumi','itra','tipulim-shonim','custom') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.reports: ~225 rows (приблизительно)
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` (`id`, `user_id`, `client_id`, `cr_id`, `name`, `settings`, `status`, `type`, `created_at`, `updated_at`) VALUES
	(1, 3, 14, NULL, 'Tipulim shonim report', '[]', 'complete', 'tipulim-shonim', '2017-06-19 14:22:45', '2017-06-19 14:49:47'),
	(2, 3, 14, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-19 14:22:47', '2017-06-19 14:49:49'),
	(3, 3, 14, NULL, 'Mas ahnasa report', '[]', 'complete', 'mas-ahnasa', '2017-06-19 14:22:50', '2017-06-19 14:48:12'),
	(6, 3, 12, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-19 14:23:19', '2017-06-19 14:47:57'),
	(7, 3, 23, NULL, 'Tipulim shonim report', '[]', 'complete', 'tipulim-shonim', '2017-06-19 14:25:33', '2017-06-19 14:48:14'),
	(8, 3, 23, NULL, 'Mas ahnasa report', '[]', 'complete', 'mas-ahnasa', '2017-06-19 14:25:35', '2017-06-19 14:49:50'),
	(9, 3, 3, 1, 'dsfsdfdf', '[]', 'complete', 'custom', '2017-06-21 05:44:57', '2017-06-21 05:44:59'),
	(10, 3, 3, 1, 'dsfsdfdf', '[]', 'complete', 'custom', '2017-06-21 05:45:00', '2017-06-21 05:45:01'),
	(11, 3, 3, NULL, 'Bituach leumi report', '[]', 'complete', 'bituach-leumi', '2017-06-21 05:45:25', '2017-06-21 20:03:39'),
	(12, 3, 3, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-21 05:45:27', '2017-06-21 12:04:55'),
	(14, 3, 3, NULL, 'Tipulim shonim report', '[]', 'complete', 'tipulim-shonim', '2017-06-21 05:55:00', '2017-06-21 20:03:40'),
	(15, 3, 3, NULL, 'Mas ahnasa report', '[]', 'complete', 'mas-ahnasa', '2017-06-21 05:59:17', '2017-06-21 20:03:40'),
	(16, 3, 6, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-21 05:59:22', '2017-06-21 20:03:46'),
	(17, 3, 6, NULL, 'Mas ahnasa report', '[]', 'complete', 'mas-ahnasa', '2017-06-21 05:59:25', '2017-06-21 20:03:46'),
	(18, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 06:01:45', '2017-06-21 20:03:41'),
	(19, 3, 3, NULL, 'Declaration of capital report', '{"date":"10 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 06:05:37', '2017-06-21 06:05:44'),
	(20, 3, 3, 10, 'custom', '[]', 'complete', 'custom', '2017-06-21 09:55:45', '2017-06-21 12:34:09'),
	(21, 3, 12, 10, 'custom', '[]', 'complete', 'custom', '2017-06-21 10:02:34', '2017-06-21 12:34:22'),
	(22, 3, 12, NULL, 'Tipulim shonim report', '[]', 'complete', 'tipulim-shonim', '2017-06-21 10:02:57', '2017-06-21 20:03:44'),
	(23, 3, 3, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 10:06:16', '2017-06-21 12:34:10'),
	(24, 3, 3, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-21 12:05:00', '2017-06-21 20:03:42'),
	(25, 3, 12, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-21 12:14:09', '2017-06-21 12:17:08'),
	(26, 3, 12, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-21 12:17:12', '2017-06-21 12:17:55'),
	(27, 3, 12, NULL, 'Itra report', '{"sum":"3"}', 'complete', 'itra', '2017-06-21 12:28:26', '2017-06-21 20:03:44'),
	(28, 3, 12, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 12:28:41', '2017-06-21 12:33:40'),
	(29, 3, 12, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 12:33:43', '2017-06-21 12:34:21'),
	(30, 3, 3, 10, 'custom', '[]', 'complete', 'custom', '2017-06-21 12:34:30', '2017-06-21 12:35:23'),
	(31, 3, 3, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 12:34:34', '2017-06-21 14:30:03'),
	(32, 3, 3, NULL, 'Itra report', '{"sum":"3"}', 'complete', 'itra', '2017-06-21 12:35:50', '2017-06-21 20:03:42'),
	(33, 3, 3, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 14:30:45', '2017-06-21 20:03:43'),
	(34, 3, 12, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 14:30:52', '2017-06-21 14:31:00'),
	(35, 3, 12, NULL, 'Annual report', '{"year":"2021"}', 'complete', 'annual-report', '2017-06-21 15:06:30', '2017-06-21 20:03:45'),
	(36, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 20:03:58', '2017-06-21 20:04:06'),
	(37, 3, 3, NULL, 'Tipulim shonim report', '[]', 'complete', 'tipulim-shonim', '2017-06-21 20:04:10', '2017-06-22 07:47:10'),
	(38, 3, 3, 10, 'custom', '[]', 'complete', 'custom', '2017-06-21 20:15:42', '2017-06-22 08:56:32'),
	(42, 3, 3, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 20:27:38', '2017-06-21 23:08:08'),
	(43, 3, 3, NULL, 'Annual report', '{"year":"2018"}', 'complete', 'annual-report', '2017-06-21 20:28:05', '2017-06-21 20:29:12'),
	(44, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 20:47:16', '2017-06-21 20:47:20'),
	(45, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 20:47:50', '2017-06-21 20:48:44'),
	(46, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 20:48:48', '2017-06-21 20:48:49'),
	(47, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 20:49:08', '2017-06-21 20:49:08'),
	(48, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 20:49:14', '2017-06-21 20:51:12'),
	(49, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-21 20:51:16', '2017-06-21 20:51:17'),
	(50, 3, 3, NULL, 'Annual report', '{"year":"2018"}', 'complete', 'annual-report', '2017-06-21 20:51:20', '2017-06-22 07:35:59'),
	(51, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:11:42', '2017-06-21 21:11:42'),
	(52, 3, 3, NULL, 'Declaration of capital report', '{"date":"29 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:11:45', '2017-06-21 21:11:45'),
	(57, 3, 3, NULL, 'Declaration of capital report', '{"date":"22 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:22:34', '2017-06-21 21:22:51'),
	(58, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:22:53', '2017-06-21 21:22:56'),
	(59, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:23:03', '2017-06-21 21:23:06'),
	(60, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:29:19', '2017-06-21 21:29:21'),
	(61, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:52:57', '2017-06-21 21:54:04'),
	(62, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:54:07', '2017-06-21 21:54:09'),
	(63, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:54:15', '2017-06-21 21:54:16'),
	(64, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:54:22', '2017-06-21 21:54:29'),
	(65, 3, 3, NULL, 'Declaration of capital report', '{"date":"29 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 21:54:33', '2017-06-21 21:56:58'),
	(66, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 22:01:58', '2017-06-21 22:02:13'),
	(67, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 22:02:15', '2017-06-21 22:03:19'),
	(68, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 22:03:22', '2017-06-21 22:03:36'),
	(69, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 22:14:09', '2017-06-21 22:14:11'),
	(70, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"30 June, 2017","percents":"1"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:21:53', '2017-06-21 22:23:05'),
	(71, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"23 June, 2017","percents":"1"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:23:13', '2017-06-21 22:26:39'),
	(72, 3, 3, NULL, 'Declaration of capital report', '{"date":"23 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-21 22:23:42', '2017-06-22 08:56:12'),
	(73, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"30 June, 2017","percents":"1"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:27:12', '2017-06-21 22:29:32'),
	(74, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"30 June, 2017","percents":"1"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:30:10', '2017-06-21 22:30:12'),
	(75, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"30 June, 2017","percents":"1"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:30:17', '2017-06-21 22:30:19'),
	(76, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"23 June, 2017","percents":"1"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:30:24', '2017-06-21 22:30:26'),
	(77, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"24 June, 2017","percents":"1"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:30:31', '2017-06-21 22:30:44'),
	(78, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"24 June, 2017","percents":"7"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 22:30:52', '2017-06-21 22:44:44'),
	(79, 3, 3, NULL, 'Itra report', '{"sum":"123"}', 'complete', 'itra', '2017-06-21 22:54:04', '2017-06-21 22:55:51'),
	(80, 3, 3, NULL, 'Itra report', '{"sum":"1231"}', 'complete', 'itra', '2017-06-21 22:56:00', '2017-06-21 22:57:17'),
	(81, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-21 22:57:24', '2017-06-21 22:57:30'),
	(82, 3, 3, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-21 23:08:10', '2017-06-22 08:56:05'),
	(83, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"30 June, 2017","percents":"2"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-21 23:09:02', '2017-06-22 08:55:55'),
	(84, 3, 6, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-22 06:35:54', '2017-06-22 08:55:51'),
	(85, 3, 8, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-22 06:36:09', '2017-06-22 08:55:50'),
	(86, 3, 14, 12, 'hello', '[]', 'complete', 'custom', '2017-06-22 07:54:55', '2017-06-22 08:55:46'),
	(87, 3, 5, 12, 'hello', '[]', 'complete', 'custom', '2017-06-22 08:57:01', '2017-06-22 14:06:38'),
	(88, 3, 3, 12, 'hello', '[]', 'complete', 'custom', '2017-06-22 09:29:50', '2017-06-22 14:06:45'),
	(89, 3, 5, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-22 10:10:26', '2017-06-22 14:06:30'),
	(90, 3, 3, NULL, 'Annual report', '{"year":"2018"}', 'complete', 'annual-report', '2017-06-22 10:32:19', '2017-06-22 10:36:42'),
	(91, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-22 10:36:47', '2017-06-22 10:36:49'),
	(92, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-22 10:36:52', '2017-06-22 14:06:42'),
	(93, 3, 3, NULL, 'Itra report', '{"sum":"44"}', 'complete', 'itra', '2017-06-22 21:12:20', '2017-06-22 21:16:41'),
	(94, 3, 3, NULL, 'Tipulim shonim report', '[]', 'complete', 'tipulim-shonim', '2017-06-22 21:13:31', '2017-06-29 06:26:22'),
	(95, 3, 3, 10, 'custom', '[]', 'complete', 'custom', '2017-06-22 21:14:01', '2017-06-29 07:47:39'),
	(96, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-22 21:14:11', '2017-06-22 22:09:21'),
	(97, 3, 3, 11, 'one more custom', '[]', 'complete', 'custom', '2017-06-22 21:14:55', '2017-06-29 06:26:39'),
	(98, 3, 3, NULL, 'Itra report', '{"sum":"3"}', 'complete', 'itra', '2017-06-22 21:25:31', '2017-06-22 21:25:34'),
	(99, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-22 21:35:15', '2017-06-29 06:26:24'),
	(100, 3, 3, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-22 22:02:22', '2017-06-29 07:47:40'),
	(101, 3, 3, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-06-22 22:02:22', '2017-06-29 06:26:41'),
	(102, 3, 3, NULL, 'Mas ahnasa report', '[]', 'complete', 'mas-ahnasa', '2017-06-22 22:02:32', '2017-06-29 06:26:42'),
	(103, 3, 3, NULL, 'Bituach leumi report', '[]', 'complete', 'bituach-leumi', '2017-06-22 22:02:38', '2017-06-29 06:26:44'),
	(104, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:02:07'),
	(105, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 06:36:18'),
	(106, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 06:36:21'),
	(107, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 06:36:26'),
	(108, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 06:38:12'),
	(109, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:15'),
	(110, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:59:29'),
	(111, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:58:26'),
	(112, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:58:27'),
	(113, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:47:42'),
	(114, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:58:49'),
	(115, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:48:17'),
	(116, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:48:09'),
	(117, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:48:07'),
	(118, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:48:06'),
	(119, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:48:20'),
	(120, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:47:52'),
	(121, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:47:51'),
	(122, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:47:54'),
	(123, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:48:10'),
	(124, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:47:56'),
	(125, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:58:36'),
	(126, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 07:47:59'),
	(127, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 June, 2017"}', 'complete', 'declaration-of-capital', '2017-06-29 06:36:32', '2017-06-29 07:48:13'),
	(128, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:05:49'),
	(129, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:05:51'),
	(130, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:17'),
	(131, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:02:32'),
	(132, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:02:30'),
	(133, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:02:24'),
	(134, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:02:21'),
	(135, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:02:18'),
	(136, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:08:53'),
	(137, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:11:31'),
	(138, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:08:59'),
	(139, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:09:02'),
	(140, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:09:24'),
	(141, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:10:03'),
	(142, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:09:48'),
	(143, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:09:50'),
	(144, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:09:51'),
	(145, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:10:05'),
	(146, 3, 3, NULL, 'Itra report', '{"sum":"1"}', 'complete', 'itra', '2017-06-22 22:02:48', '2017-06-29 08:28:07'),
	(147, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:20'),
	(148, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:41'),
	(149, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:07'),
	(150, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:09'),
	(151, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:13'),
	(152, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:11'),
	(153, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:42'),
	(154, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:14'),
	(155, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:43'),
	(156, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:17'),
	(157, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:18'),
	(158, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:23'),
	(159, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:19'),
	(160, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:21'),
	(161, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:23'),
	(162, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:25'),
	(163, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:26'),
	(164, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:27'),
	(165, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:28'),
	(166, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:29'),
	(167, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:30'),
	(168, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:31'),
	(169, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:33'),
	(170, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:34'),
	(171, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:47'),
	(172, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:48'),
	(173, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:49'),
	(174, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:50'),
	(175, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:52'),
	(176, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:10:55'),
	(177, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:05'),
	(178, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:07'),
	(179, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:08'),
	(180, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:09'),
	(181, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:10'),
	(182, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:11'),
	(183, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:12'),
	(184, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:13'),
	(185, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:13'),
	(186, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:14'),
	(187, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:14'),
	(188, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:15'),
	(189, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:16'),
	(190, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:27'),
	(191, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:11:28'),
	(192, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:25'),
	(193, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:26'),
	(194, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:27'),
	(195, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:29'),
	(196, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:29'),
	(197, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 09:26:45'),
	(198, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 08:12:31'),
	(199, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:33'),
	(200, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:32'),
	(201, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 08:41:44'),
	(202, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:34'),
	(203, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 08:58:18'),
	(204, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:36'),
	(205, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:39'),
	(206, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:38'),
	(207, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:38'),
	(208, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:40'),
	(209, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:40'),
	(210, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-06-29 08:28:41'),
	(211, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:41'),
	(212, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:42'),
	(213, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:44'),
	(214, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:45'),
	(215, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:46'),
	(216, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:47'),
	(217, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:47'),
	(218, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:49'),
	(219, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:49'),
	(220, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:51'),
	(221, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:51'),
	(222, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:54'),
	(223, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:54'),
	(224, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:55'),
	(225, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:56'),
	(226, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:57'),
	(227, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:09:58'),
	(228, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:10:00'),
	(229, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:10:00'),
	(230, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-06-28 05:40:04', '2017-07-13 11:10:03'),
	(231, 3, 3, 13, 'sdfsdf', '[]', 'complete', 'custom', '2017-06-29 08:12:15', '2017-06-29 08:12:48'),
	(232, 3, 3, 14, 'qweqwe', '[]', 'complete', 'custom', '2017-06-29 09:22:56', '2017-07-25 06:34:25'),
	(233, 3, 3, 13, 'sdfsdf', '[]', 'pending', 'custom', '2017-06-29 09:23:55', '2017-06-29 09:23:55'),
	(234, 3, 3, NULL, 'Tipulim shonim report', '[]', 'complete', 'tipulim-shonim', '2017-06-29 09:24:01', '2017-07-25 06:28:20'),
	(235, 3, 3, NULL, 'Bituach leumi report', '[]', 'complete', 'bituach-leumi', '2017-06-29 09:24:11', '2017-07-25 06:32:45'),
	(236, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"15 June, 2017","percents":"10"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-29 09:24:46', '2017-06-29 09:25:04'),
	(237, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"21 June, 2018","percents":"10"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-06-29 09:25:21', '2017-07-17 06:52:13'),
	(238, 3, 3, NULL, 'Mam report', '[]', 'complete', 'mam', '2017-07-13 08:29:56', '2017-07-25 06:28:12'),
	(240, 3, 3, 12, 'hello', '[]', 'complete', 'custom', '2017-07-13 08:37:03', '2017-07-17 08:24:30'),
	(241, 3, 3, 11, 'one more custom', '[]', 'complete', 'custom', '2017-07-13 08:37:11', '2017-07-17 08:24:34'),
	(242, 3, 3, 15, 'custom', '[]', 'complete', 'custom', '2017-07-13 08:39:13', '2017-07-25 06:33:55'),
	(243, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-07-13 08:41:59', '2017-07-13 11:10:03'),
	(244, 3, 3, NULL, 'Itra report', '{"sum":"3234"}', 'complete', 'itra', '2017-07-13 10:29:42', '2017-07-13 10:29:58'),
	(245, 3, 3, NULL, 'Itra report', '{"sum":"324234"}', 'complete', 'itra', '2017-07-13 10:30:01', '2017-07-13 10:30:16'),
	(246, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'complete', 'annual-report', '2017-07-14 08:40:59', '2017-07-14 08:41:06'),
	(247, 3, 3, NULL, 'Annual report', '{"year":"2017"}', 'pending', 'annual-report', '2017-07-17 06:27:13', '2017-07-17 06:27:13'),
	(248, 3, 3, NULL, 'Declaration of capital report', '{"date":"4 July, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:27:36', '2017-07-17 06:33:45'),
	(249, 3, 3, NULL, 'Declaration of capital report', '{"date":"26 June, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:27:39', '2017-07-17 06:46:33'),
	(250, 3, 3, NULL, 'Declaration of capital report', '{"date":"31 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:27:40', '2017-07-17 06:47:02'),
	(251, 3, 3, NULL, 'Declaration of capital report', '{"date":"3 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:27:41', '2017-07-17 06:51:20'),
	(252, 3, 3, NULL, 'Declaration of capital report', '{"date":"3 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:27:43', '2017-07-17 06:51:51'),
	(253, 3, 3, NULL, 'Declaration of capital report', '{"date":"2 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:27:43', '2017-07-17 07:38:11'),
	(254, 3, 3, NULL, 'Declaration of capital report', '{"date":"5 June, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:27:47', '2017-07-17 07:38:24'),
	(255, 3, 3, NULL, 'Declaration of capital report', '{"date":"30 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:34:12', '2017-07-17 07:38:27'),
	(256, 3, 3, NULL, 'Declaration of capital report', '{"date":"2 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:34:15', '2017-07-17 07:38:29'),
	(257, 3, 3, NULL, 'Declaration of capital report', '{"date":"8 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:34:18', '2017-07-17 07:38:32'),
	(258, 3, 3, NULL, 'Declaration of capital report', '{"date":"17 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:34:25', '2017-07-17 07:38:36'),
	(259, 3, 3, NULL, 'Declaration of capital report', '{"date":"7 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:35:06', '2017-07-17 07:38:39'),
	(260, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"20 June, 2018","percents":"3"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-07-17 06:52:19', '2017-07-17 06:52:50'),
	(261, 3, 3, NULL, 'Declaration of capital report', '{"date":"10 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 06:52:59', '2017-07-17 07:38:42'),
	(262, 3, 3, NULL, 'Declaration of capital report', '{"date":"2 May, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 07:38:14', '2017-07-17 07:38:20'),
	(263, 3, 3, NULL, 'Declaration of capital report', '{"date":"5 July, 2017"}', 'complete', 'declaration-of-capital', '2017-07-17 07:38:49', '2017-07-17 07:38:53'),
	(264, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"18 July, 2017","percents":"3"}', 'complete', 'ishur-nikui-mas-bamakor', '2017-07-17 08:15:49', '2017-07-17 08:23:21'),
	(265, 3, 3, NULL, 'Ishur nikui mas bamakor report', '{"date":"25 July, 2017","percents":"4"}', 'pending', 'ishur-nikui-mas-bamakor', '2017-07-17 08:23:38', '2017-07-17 08:23:38'),
	(266, 3, 3, NULL, 'Itra report', '{"sum":"6"}', 'pending', 'itra', '2017-07-18 07:42:54', '2017-07-18 07:42:54'),
	(267, 3, 3, 11, 'one more custom', '[]', 'pending', 'custom', '2017-07-25 06:18:29', '2017-07-25 06:18:29');
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.roles: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
	(1, 'users', 'Users', '{"user":true}', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(2, 'admins', 'Admins', '{"admin":true,"user":true}', '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(3, 'superadmins', 'SuperAdmins', '{"superadmin":true,"admin":true,"user":true}', '2017-06-19 14:22:13', '2017-06-19 14:22:13');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.role_users
CREATE TABLE IF NOT EXISTS `role_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.role_users: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
	(1, 3, '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(2, 2, '2017-06-19 14:22:13', '2017-06-19 14:22:13'),
	(3, 1, '2017-06-19 14:22:13', '2017-06-19 14:22:13');
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.throttle
CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.throttle: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'global', NULL, '2017-06-22 13:17:21', '2017-06-22 13:17:21'),
	(2, NULL, 'ip', '192.168.10.1', '2017-06-22 13:17:21', '2017-06-22 13:17:21'),
	(3, 1, 'user', NULL, '2017-06-22 13:17:21', '2017-06-22 13:17:21'),
	(4, NULL, 'global', NULL, '2017-06-22 13:17:28', '2017-06-22 13:17:28'),
	(5, NULL, 'ip', '192.168.10.1', '2017-06-22 13:17:28', '2017-06-22 13:17:28'),
	(6, 1, 'user', NULL, '2017-06-22 13:17:28', '2017-06-22 13:17:28');
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.todo_items
CREATE TABLE IF NOT EXISTS `todo_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('pending','complete') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.todo_items: ~58 rows (приблизительно)
/*!40000 ALTER TABLE `todo_items` DISABLE KEYS */;
INSERT INTO `todo_items` (`id`, `report_id`, `user_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
	(1, 12, 3, '123', 'pending', '2017-06-21 12:06:07', '2017-06-21 12:06:07'),
	(2, 12, 3, 'dsa', 'pending', '2017-06-21 12:06:32', '2017-06-21 12:06:32'),
	(3, 12, 3, 'fdsa', 'pending', '2017-06-21 12:08:11', '2017-06-21 12:08:11'),
	(4, 24, 3, 'fda', 'pending', '2017-06-21 12:08:39', '2017-06-21 12:08:39'),
	(5, 24, 3, 'asdfsadf', 'pending', '2017-06-21 12:10:40', '2017-06-21 12:10:40'),
	(6, 24, 3, '123', 'pending', '2017-06-21 12:13:46', '2017-06-21 12:13:46'),
	(7, 26, 3, 'sdf', 'pending', '2017-06-21 12:17:15', '2017-06-21 12:17:15'),
	(8, 21, 3, 'hfghfgh', 'pending', '2017-06-21 12:27:53', '2017-06-21 12:27:53'),
	(9, 21, 3, 'gsdfg', 'pending', '2017-06-21 12:28:34', '2017-06-21 12:28:34'),
	(10, 29, 3, 'ewqerwer', 'pending', '2017-06-21 12:33:46', '2017-06-21 12:33:46'),
	(11, 29, 3, 'rqwer', 'pending', '2017-06-21 12:33:49', '2017-06-21 12:33:49'),
	(12, 31, 3, '123', 'pending', '2017-06-21 12:35:26', '2017-06-21 12:35:26'),
	(13, 31, 3, '321', 'pending', '2017-06-21 12:35:41', '2017-06-21 12:35:41'),
	(14, 37, 3, '123', 'complete', '2017-06-21 20:04:13', '2017-06-22 07:47:04'),
	(15, 37, 3, '321', 'complete', '2017-06-21 20:11:56', '2017-06-22 07:46:53'),
	(16, 37, 3, 'qwe', 'complete', '2017-06-21 20:11:59', '2017-06-22 07:45:04'),
	(17, 37, 3, 'ewq', 'complete', '2017-06-21 20:12:01', '2017-06-22 07:45:04'),
	(18, 37, 3, 'asddsa', 'complete', '2017-06-21 20:12:39', '2017-06-22 07:45:02'),
	(19, 38, 3, '123', 'complete', '2017-06-21 20:15:44', '2017-06-22 07:41:12'),
	(20, 39, 3, '123', 'pending', '2017-06-21 20:27:08', '2017-06-21 20:27:08'),
	(21, 40, 3, 'dsa', 'pending', '2017-06-21 20:27:16', '2017-06-21 20:27:16'),
	(22, 41, 3, 'asd', 'pending', '2017-06-21 20:27:21', '2017-06-21 20:27:21'),
	(23, 42, 3, 'fds', 'pending', '2017-06-21 20:27:42', '2017-06-21 20:27:42'),
	(24, 42, 3, '123', 'pending', '2017-06-21 22:59:45', '2017-06-21 22:59:45'),
	(25, 42, 3, '321', 'pending', '2017-06-21 23:07:31', '2017-06-21 23:07:31'),
	(26, 42, 3, '312', 'pending', '2017-06-21 23:08:01', '2017-06-21 23:08:01'),
	(27, 82, 3, 'sdfsdf', 'complete', '2017-06-21 23:08:12', '2017-06-22 07:27:16'),
	(28, 84, 3, '234234', 'complete', '2017-06-22 06:35:57', '2017-06-22 07:27:15'),
	(29, 85, 3, '4324234234', 'complete', '2017-06-22 06:36:12', '2017-06-22 07:26:04'),
	(30, 82, 3, '3213123', 'complete', '2017-06-22 07:27:29', '2017-06-22 07:27:41'),
	(31, 82, 3, '123123', 'complete', '2017-06-22 07:27:32', '2017-06-22 07:27:42'),
	(32, 82, 3, '123123', 'complete', '2017-06-22 07:27:34', '2017-06-22 07:27:42'),
	(33, 82, 3, '123123123', 'complete', '2017-06-22 07:27:37', '2017-06-22 07:27:43'),
	(34, 82, 3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci earum id inventore ipsam mollitia nesciunt odit officia placeat quae quam qui, quis, voluptatum.', 'complete', '2017-06-22 07:28:28', '2017-06-22 07:37:28'),
	(35, 86, 3, 'asdfsadf', 'complete', '2017-06-22 07:54:59', '2017-06-22 07:55:12'),
	(36, 86, 3, 'asdf', 'complete', '2017-06-22 07:55:01', '2017-06-22 07:55:13'),
	(37, 86, 3, '123', 'complete', '2017-06-22 08:55:18', '2017-06-22 08:55:34'),
	(38, 87, 3, 'qweqwe', 'pending', '2017-06-22 08:57:04', '2017-06-22 08:57:04'),
	(39, 88, 3, '12312312', 'complete', '2017-06-22 09:29:56', '2017-06-22 10:28:15'),
	(40, 88, 3, '123123', 'complete', '2017-06-22 09:29:59', '2017-06-22 10:28:17'),
	(41, 88, 3, 'asdfasdf', 'pending', '2017-06-22 09:30:32', '2017-06-22 09:30:32'),
	(42, 87, 3, 'dfasdf', 'pending', '2017-06-22 10:20:37', '2017-06-22 10:20:37'),
	(43, 87, 3, 'asdfas', 'pending', '2017-06-22 10:20:39', '2017-06-22 10:20:39'),
	(44, 100, 3, 'fgsdfgdfg', 'complete', '2017-06-22 22:11:41', '2017-06-22 22:19:48'),
	(45, 100, 3, 'dsfgdsfg', 'complete', '2017-06-22 22:11:43', '2017-06-22 22:19:49'),
	(46, 100, 3, 'fasdfsadf', 'complete', '2017-06-28 05:40:41', '2017-06-28 05:40:56'),
	(47, 231, 3, 'asldkfjasl;jkdfl', 'complete', '2017-06-29 08:12:19', '2017-06-29 08:12:43'),
	(48, 231, 3, 'dgdsfg', 'complete', '2017-06-29 08:12:22', '2017-06-29 08:12:44'),
	(49, 232, 3, 'asdfasdf', 'pending', '2017-06-29 09:23:10', '2017-06-29 09:23:10'),
	(50, 232, 3, 'asdfsadf', 'pending', '2017-06-29 09:23:13', '2017-06-29 09:23:13'),
	(51, 234, 3, 'dfsdf', 'complete', '2017-07-17 07:49:20', '2017-07-17 08:05:07'),
	(52, 234, 3, 'sdfsdf', 'complete', '2017-07-17 07:49:31', '2017-07-17 08:05:08'),
	(53, 234, 3, 'sdfsdf', 'complete', '2017-07-17 07:49:32', '2017-07-17 08:05:30'),
	(54, 234, 3, 'sdfsfd', 'complete', '2017-07-17 07:50:37', '2017-07-17 13:20:17'),
	(55, 234, 3, 'sdfsdf', 'complete', '2017-07-17 07:50:40', '2017-07-17 08:05:13'),
	(56, 234, 3, 'fasfs', 'complete', '2017-07-17 07:53:47', '2017-07-17 13:20:14'),
	(57, 267, 3, 'вафыва', 'complete', '2017-07-25 06:18:32', '2017-07-25 06:22:03'),
	(58, 267, 3, 'фываыва', 'pending', '2017-07-25 06:18:34', '2017-07-25 06:18:34');
/*!40000 ALTER TABLE `todo_items` ENABLE KEYS */;

-- Дамп структуры для таблица accountant.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы accountant.users: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `last_login`, `first_name`, `last_name`, `phone`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin@admin.com', '$2y$10$zKCSnLOpAYFQ8jbu74AFlucNcSUN6DtEFbOhR5vJYhTo5UdPpOeU.', NULL, '2017-06-22 13:17:38', 'Superadmin', '', NULL, '2017-06-19 14:22:13', '2017-06-22 13:17:38'),
	(2, 'admin@admin.com', '$2y$10$lI3akbzeevFVNALz4xRLGeuVwdcsGDvwK988c6vtJQUGUgFm4fFfK', NULL, '2017-06-21 11:04:07', 'Admin', '', NULL, '2017-06-19 14:22:13', '2017-06-21 11:04:07'),
	(3, 'grabli1989@gmail.com', '$2y$10$.45zdpvtAWBmrJcrX/at8.w3PV8GRBQRYiLCFE0/kIi4iBytqQrxa', NULL, '2017-07-25 06:12:45', 'Denis', '', NULL, '2017-06-19 14:22:13', '2017-07-25 06:12:45');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
