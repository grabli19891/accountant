<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminPanel extends Controller
{
    // Main page
    public function index()
    {
        return view('admin-panel.info');
    }
}
