<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RoleCreateRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Sentinel::getRoleRepository()->get();
        $data = [
            'roles' => $roles
        ];
        return view('admin-panel.roles.roles')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-panel.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleCreateRequest $request)
    {
        $data = $request->only('slug', 'name', 'permissions');
        $data['permissions'] = json_decode($data['permissions'], true);

        if ($role = Sentinel::getRoleRepository()->createModel()->create($data)) {
            return redirect()->route('roles.index')->withSuccess('New role created!');
        }

        return redirect()->back()->withInput()->withErrors('Something went wrong!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($role = Sentinel::findRoleById($id)) {
            return view('admin-panel.roles.edit')->with(['role' => $role]);
        }

        return redirect()->back()->withErrors('Role not found!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, $id)
    {
        $data = $request->only('slug', 'name', 'permissions');
        $data['permissions'] = json_decode($data['permissions'], true);

        if ($role = Role::find($id)->update($data)) {
            return redirect()->route('roles.index')->withSuccess('Role updated!');
        }

        return redirect()->back()->withInput()->withErrors('Something went wrong!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($role = Sentinel::findRoleById($id)) {
            $role->delete();
            return redirect()->back()->withSuccess('Delete success');
        }

        return redirect()->back()->withErrors('Something went wrong!');
    }
}
