<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Sentinel::getUserRepository()->get();
        $data = [
            'users' => $users
        ];
        return view('admin-panel.users.users')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-panel.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        $data = $request->only('email', 'first_name', 'last_name', 'phone', 'password');
        if (Sentinel::registerAndActivate($data)) {
            if ($user->hasAccess('superadmin')) {
                $roles = $request->get('roles');
                $user->roles()->sync($roles);
            } else {
                $user->roles()->attach(Sentinel::findRoleByName('Users'));
            }
            return redirect()->route('users.index')->withSuccess('User created!');
        }
        return redirect()->back()->withInput()->withErrors('Something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($user = User::find($id)) {
            return view('admin-panel.users.edit')->with(['user' => $user]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        /** @var User $user */
        $user = Sentinel::findById($id);
        $data = $request->only('email', 'first_name', 'last_name', 'phone', 'password');
        if ($user->update($data)) {

            if ($user->hasAccess('superadmin')) {
                $roles = $request->get('roles');
                $user->roles()->sync($roles);
            }

            return redirect()->route('users.index')->withSuccess('User update success!');
        }
        return redirect()->route('users.index')->withErrors('Something went wrong!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($user = Sentinel::findById($id)) {
            $user->delete();
            return redirect()->back()->withSuccess('Delete success');
        }

        return redirect()->back()->withErrors('Something went wrong!');
    }
}
