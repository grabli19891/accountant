<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Activation;
use Sentinel;

class ActivationController extends Controller
{
   public function activate($id, $code)
   {
           $user = Sentinel::findById($id);

           if ( ! Activation::complete($user, $code))
           {
               return redirect()->route('login')
                   ->withErrors('Invalid or expired activation code.');
           }

           return redirect()->route('login')
               ->withSuccess('Account activated.');
   }
}
