<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\ResetFinishRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Reminder;
use Sentinel;

class ResetController extends Controller
{
    public function index()
    {
        return view('auth.reset');
    }

    public function stepOne(Request $request)
    {
        $email = $request->get('email');
        $user = Sentinel::findByCredentials(compact('email'));
        if (!$user)
        {
            return redirect()->back()
                ->withInput()
                ->withErrors('No user with that email address belongs in our system.');
        }
         $reminder = Reminder::exists($user) ?: Reminder::create($user);
         $code = $reminder->code;
         $sent = Mail::send('emails.reminder', compact('user', 'code'), function($m) use ($user)
         {
         	$m->to($user->email)->subject('Reset your account password.');
         });
         if ($sent === 0)
         {
         	return redirect()->route('register')
         		->withErrors('Failed to send reset password email.');
         }
        return redirect()->route('wait');
    }

    /**
     * Второй шаг, переход по ссылке пришедшей на email
     *
     * @param  $id
     * @param  $code
     * @return  mixed
     */
    public function stepTwo($id, $code)
    {
        return view('auth.reset_complete');
    }

    /**
     * Последний шаг, смена пароля
     *
     * @param ResetFinishRequest $request
     * @param $id
     * @param $code
     * @return mixed
     */
    public function stepFinish(ResetFinishRequest $request, $id, $code)
    {
        $password = $request->get('password');
        $user = Sentinel::findById($id);
        if (!$user)
        {
            return redirect()->back()
                ->withInput()
                ->withErrors('The user no longer exists.');
        }
        if (!Reminder::complete($user, $code, $password)) {
            return redirect()->route('login')
                ->withErrors('Invalid or expired reset code.');
        }

        return redirect()->route('login')
            ->withSuccess("Password Reset.");
    }
}
