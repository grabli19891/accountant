<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\SessionCreateRequest;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Sentinel;

class SessionsController extends Controller
{
    public function create()
    {
        return view('auth.login');
    }

    public function store(SessionCreateRequest $request)
    {
        $credentials = $request->only('remember', 'email', 'password');
        try {
            $remember = (bool) $credentials['remember'];

            if (Sentinel::authenticate($credentials, $remember))
            {
                return redirect()->route('dashboard.index')->withSuccess('You logged in');
            }

            $errors = 'Invalid login or password.';
        }

        catch (NotActivatedException $e)
        {
            $errors = 'Account is not activated!';
            return redirect()->route('reactivate', ['id'=>$e->getUser()->getUserId()])->withErrors($errors);
        }

        catch (ThrottlingException $e)
        {
            $delay = $e->getDelay();
            $errors = "Your account is blocked for {$delay} second(s).";
        }

        return redirect()->back()->withInput()->withErrors($errors);

    }

    public function destroy()
    {
        Sentinel::logout();
        return redirect()->route('default');
    }
}
