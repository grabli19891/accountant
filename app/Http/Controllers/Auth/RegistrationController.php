<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use Sentinel;
use Activation;
use Mail;

class RegistrationController extends Controller
{
    public function create() 
    {
        return view('auth.register');
    }
    
    public function store(RegistrationRequest $request)
    {
        $credentials = $request->only('email', 'password', 'first_name', 'last_name', 'phone');

        /** @var User $user */
        if ($user = Sentinel::register($credentials))
        {
            $userRole = Sentinel::findRoleByName('Users');
            $userRole->users()->attach($user);
            $activation = Activation::create($user);
            $code = $activation->code;

            $sent = Mail::send('emails.activate', compact('user', 'code'), function($m) use ($user) {
                $m->to($user->email)->subject('Activate Your Account');
            });

            if ($sent === 0) {
                return redirect()->back()->withInput()->withErrors('Failed to send activation email.');
            }

            return redirect()->route('wait')->withSuccess('Your accout was successfully created.')
                ->with('userId', $user->getUserId());
        }

        return redirect()->back()->withInput()->withErrors('An error occured while registering.');
    }

    public function reactivate($id)
    {
        $user = Sentinel::findById($id);

        if (!$user)
        {
            return redirect()->route('login');
        }

        $activation = Activation::exists($user) ?: Activation::create($user);

        $code = $activation->code;

        $sent = Mail::send('emails.activate', compact('user', 'code'), function($m) use ($user)
        {
            $m->to($user->email)->subject('Activate Your Account');
        });

        if ($sent === 0)
        {
            return redirect()->route('registration.create')
                ->withErrors('Failed to send activation email.');
        }

        return redirect()->route('wait')
            ->withSuccess('Very shortly, you will receive an email with instructions on how to continue.');
    }
}