<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function updateProfile(UpdateProfileRequest $request)
    {
        $input = $request->only(['first_name', 'last_name', 'phone', 'password']);
        $user = User::current();
        if (!$user->update($input)) {
            return redirect()->back()->with('tab', 'profile')->withErrors('Something went wrong');
        }
        return redirect()->back()->with('tab', 'profile')->withSuccess('Profile updated');
    }
}
