<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\CustomReport;
use App\Models\User;
use function foo\func;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::current();
        $clients = $user->clients->filter(function ($c) {
            return $c->reports()->where('status', 'pending')->get()->isNotEmpty();
        });
        $reports = $user->reports()->where('status', 'pending')->get();
        return view('dashboard.index')->with(['reports' => $reports, 'clients' => $clients]);
    }

    /**
     * @param Request $request
     * @param string $tab
     * @return $this
     */
    public function settings(Request $request, $tab = null)
    {
        $user = User::current();
        if ($tab) {
            $request->session()->put('tab', $tab);
        }
        $customReports = $user->customReports()->get();
        return view('dashboard.settings')->with(['customReports' => $customReports, 'user' => $user]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function customReportDelete($id)
    {
        $user = User::current();
        if (!$customReport = $user->customReports()->find($id)) {
            return redirect()->back()->withErrors('Custom report not found');
        }
        /** @var CustomReport $customReport */
        $customReport->reports()->where('status', 'pending')->delete();

        if ($customReport->delete()) {
            return redirect()->back()->withSuccess('Custom report deleted!');
        }
    }

}
