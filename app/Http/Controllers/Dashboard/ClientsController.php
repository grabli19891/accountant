<?php

namespace App\Http\Controllers\Dashboard;

use App\Exceptions\TooFewArgumentsToReportException;
use App\Http\Requests\AttachReportRequest;
use App\Http\Requests\ClientStoreRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Http\Requests\CreateCustomReportRequest;
use App\Http\Requests\ReportCompleteRequest;
use App\Models\Client;
use App\Models\CustomReport;
use App\Models\Report;
use App\Models\User;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::current();
        $clients = $user->clients;
        return view('dashboard.clients.index')->with(['clients' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = new Client();
        return view('dashboard.clients.create')->with(['client' => $client]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientStoreRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientStoreRequest $request)
    {
        $user = User::current();
        $input = $request->only([
            'number',
            'name',
            'passport',
            'tiknikuim',
            'phone',
            'email'
        ]);

        $client = (new Client())
            ->fill($input)
            ->setAttribute('user_id', $user->getAttribute('id'))
            ->save();
        if ($client) {
            return redirect()->route('clients.index')->withSuccess('Client created!');
        }
        return redirect()->back()->withErrors('Something went wrong...')->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($client = Sentinel::getUser()->clients()->find($id)) {
            return view('dashboard.clients.show')->with(['client' => $client, 'user' => User::current()]);
        }
        return redirect()->back()->withErrors('Client not found');
    }

//    /**
//     * @param $id  integer
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function getClient(Request $request)
//    {
//        $id = (integer)$request->get('client_id');
//        if ($client = Sentinel::getUser()->clients()->find($id)) {
//            return response()->json(['client' => $client], 200);
//        }
//        return response()->json('Client not found', 404);
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($client = Sentinel::getUser()->clients()->find($id)) {
            return view('dashboard.clients.edit')->with(['client' => $client]);
        }
        return redirect()->back()->withErrors('Client not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClientUpdateRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientUpdateRequest $request, $id)
    {
        $user = User::current();
        $input = $request->only([
            'number',
            'name',
            'passport',
            'tiknikuim',
            'phone',
            'email'
        ]);

        $client = $user->clients()->find($id)->fill($input)->save();
        if ($client) {
            return redirect()->route('clients.show', $id)->withSuccess('Client updated!');
        }
        return redirect()->back()->withErrors('Something went wrong...')->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::current();
        if ($client = $user->clients()->find($id)) {
            $client->delete();
            return redirect()->route('clients.index')->withSuccess('Client deleted!');
        }
        return redirect()->back()->withErrors('Client not found');
    }

    /**
     * @param AttachReportRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachReport(AttachReportRequest $request, $id)
    {
        $user = User::current();
        if (! $client = $user->clients()->find($id)) {
            return response()->json(false, 404);
        }
        $type = $request->get('type');
        $settings = $request->get('settings');
        /** @var Client $client */
        try {
            $report = (new Report())->fillReport($client, $type, $settings);
            if ($report) {
                $report->setAttribute('user_id', $user->id);
                $report->save();
            }
        } catch (TooFewArgumentsToReportException $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 500);
        }

        return response()->json($report, 201);
    }

    public function reportComplete(ReportCompleteRequest $request, $id)
    {
        $user = User::current();
        if (! $client = $user->clients()->find($id)) {
            return response()->json(false, 404);
        }
        $type = $request->get('type');
        $report_id = $request->get('report_id');
        /** @var Client $client */
        if ($client->getPendingReport($type)->find($report_id)->complete()) {
            return response()->json('report complete', 201);
        }
        return response()->json(false, 200);
    }
}
