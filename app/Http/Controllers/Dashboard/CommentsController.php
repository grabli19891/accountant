<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\CommentsStoreRequest;
use App\Models\Client;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentsStoreRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentsStoreRequest $request)
    {
        $user = User::current();
        $report_id = $request->get('report_id');
        $client_id = $request->get('client_id');
        $comment_text = $request->get('comment');
        if (!$report = $user->clients()->find($client_id)->reports()->find($report_id)) {
            return response()->json('Report not found', 404);
        }
        if (! $comment = Comment::create(['user_id' => $user->id, 'parent_id' => $report->id, 'comment' => $comment_text])) {
            return response()->json('Something went wrong', 500);
        }
        return response()->json($comment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::current();
        /** @var Comment $comment */
        if (! $comment = $user->comments()->find($id)) {
            return response()->json('Comment not found', 404);
        }
        if ($comment->delete()) {
            return response()->json(true, 200);
        }
        return response()->json('Something went wrong', 500);
    }
}
