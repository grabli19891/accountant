<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\CreateCustomReportRequest;
use App\Models\CustomReport;
use App\Models\Report;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param null $reports
     * @param null $stats
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $reports = null, $stats = null)
    {
        $user = User::current();
        if ($type = $request->get('type')) {
            $reports = $user->reportsByType($type)->where('status', 'pending')->get();
        }
        if ($type && $reports->isEmpty()) {
            $reports = $user->reportsByName($type)->where('status', 'pending')->get();
        }
        $reportsSelect = Report::getArrayReports($user);
        return view('dashboard.reports.index')->with(['reportsSelect' => $reportsSelect, 'reports' => $reports, 'user' => $user]);
    }

    /**
     * @param CreateCustomReportRequest $request
     * @return $this
     */
    public function createCustomReport(CreateCustomReportRequest $request)
    {
        $user = User::current();
        $name = $request->get('name');

        if ($cr = CustomReport::create(['user_id' => $user->id, 'name' => $name])) {
            return redirect()->back()->with('tab', 'custom_reports')->withSuccess('Report created!');
        }
        return redirect()->back()->with('tab', 'add_custom_report')->withErrors('Something went wrong');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
