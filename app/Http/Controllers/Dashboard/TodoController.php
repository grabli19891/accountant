<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\TodoStoreRequest;
use App\Models\TodoItem;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TodoStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoStoreRequest $request)
    {
        $user = User::current();
        $report_id = $request->get('report_id');
        $client_id = $request->get('client_id');
        $name = $request->get('name');
        if (!$report = $user->clients()->find($client_id)->reports()->find($report_id)) {
            return response()->json('Report not found', 404);
        }
        if (! $todo = TodoItem::create(['user_id' => $user->id, 'report_id' => $report->id, 'name' => $name, 'status' => 'pending'])) {
            return response()->json('Something went wrong', 500);
        }
        return response()->json($todo, 201);
    }

    public function todoComplete($id)
    {
        $user = User::current();
        /** @var TodoItem $todo */
        $todo = $user->todoItems()->find($id);
        if ($todo->complete()) {
            return response()->json($todo, 200);
        }
        return response()->json(false, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::current();
        if (! $todo = $user->reports()->find($id)) {
            return response()->json('Comment not found', 404);
        }
        if ($todo->delete()) {
            return response()->json(true, 200);
        }
        return response()->json('Something went wrong', 500);
    }
}
