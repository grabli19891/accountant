<?php

namespace App\Http\Middleware;

use Closure;

class AllowIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * Allow if admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Sentinel::getUser();
        if (!$user->hasAnyAccess('admin')) {
            return redirect()->route('login')->withErrors('Access denied!');
        }
        return $next($request);
    }
}
