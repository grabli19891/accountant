<?php

namespace App\Models;

use App\Exceptions\TooFewArgumentsToReportException;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Report
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $client_id
 * @property string $name
 * @property array $settings
 * @property string $status
 * @property string $type
 * @property-read \App\Models\Client $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereSettings($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereType($value)
 * @property int $user_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereUserId($value)
 * @property int $cr_id
 * @property-read \App\Models\CustomReport $customReport
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TodoItem[] $todoItems
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Report whereCrId($value)
 */
class Report extends Model
{
    protected $fillable = [
        'user_id', 'client_id', 'cr_id', 'name', 'settings', 'status', 'type'
    ];

    protected $casts = [
        'settings' => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id');
    }

    public function customReport()
    {
        return $this->belongsTo(CustomReport::class, 'cr_id');
    }

    public function todoItems()
    {
        return $this->hasMany(TodoItem::class, 'report_id', 'id');
    }


    public static function getArrayReports($user)
    {
        $customs = static::where('type', 'custom')->where('user_id', $user->id)->get()->unique('name')->pluck('name', 'name')->all();
        return array_merge([
            'annual-report' => 'דוח שנתי',
            'declaration-of-capital' => 'הצהרת הון',
            'ishur-nikui-mas-bamakor' => 'אישור ניכוי מס במקור',
            'mam' => 'מע"מ',
            'mas-ahnasa' => 'מס הכנסה',
            'bituach-leumi' => 'ביטוח לאומי',
            'itra' => 'יתרה',
            'tipulim-shonim' => 'טיפולים שונים',
        ], $customs);
    }

    /**
     * @param Collection $reports
     * @return Collection
     */
    public static function sorta(Collection $reports)
    {
        $array = [
            'annual-report' => null,
            'declaration-of-capital' => null,
            'ishur-nikui-mas-bamakor' => null,
            'mam' => null,
            'mas-ahnasa' => null,
            'bituach-leumi' => null,
            'itra' => null,
            'tipulim-shonim' => null,
            'custom' => null
        ];
        /** @var Collection $reports */

        $reports->each(function ($report) use (&$array) {
            /** @var Report $report */
            if ($report->type != 'custom') {
                $array[$report->type] = $report;
            }
        });

        $reports = collect($array)->values();

        return $reports;
    }

    public function nameRep($type)
    {
        $names = [
            'annual-report' => 'דוח שנתי',
            'declaration-of-capital' => 'הצהרת הון',
            'ishur-nikui-mas-bamakor' => 'אישור ניכוי מס במקור',
            'mam' => 'מע"מ',
            'mas-ahnasa' => 'מס הכנסה',
            'bituach-leumi' => 'ביטוח לאומי',
            'itra' => 'יתרה',
            'tipulim-shonim' => 'טיפולים שונים',
        ];
        if (isset($names[$type])) {
            return $names[$type];
        }
        return false;
    }

    /**
     * @param Client $client
     * @param $year int
     * @return array
     */
    private function createAnnualReport(Client $client, $year)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('annual-report'),
            'settings' => [
                'year' => ['data' => $year, 'title' => 'שנה'],
            ],
            'status' => 'pending',
            'type' => 'annual-report'
        ];
        return $fills;
    }

    /**
     * @param Client $client
     * @param $date string
     * @return array
     */
    private function createDeclarationOfCapitalReport(Client $client, $date)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('declaration-of-capital'),
            'settings' => [
                'date' => ['data' => $date, 'title' => 'תאריך'],
            ],
            'status' => 'pending',
            'type' => 'declaration-of-capital'
        ];
        return $fills;
    }

    /**
     * @param Client $client
     * @param $date string
     * @param $percents int
     * @return array
     */
    private function createIshurNikuiMasBamakorReport(Client $client, $date, $percents)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('ishur-nikui-mas-bamakor'),
            'settings' => [
                'date' => [
                    'data' => $date,
                    'title' => 'תאריך'
                ],
                'percents' => [
                    'data' => $percents,
                    'title' => 'באחוזים'
                ],
            ],
            'status' => 'pending',
            'type' => 'ishur-nikui-mas-bamakor'
        ];
        return $fills;
    }

    /**
     * @param Client $client
     * @return array
     */
    private function createMamReport(Client $client)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('mam'),
            'settings' => [],
            'status' => 'pending',
            'type' => 'mam'
        ];
        return $fills;
    }

    /**
     * @param Client $client
     * @return array
     */
    private function createMasAhnasaReport(Client $client)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('mas-ahnasa'),
            'settings' => [],
            'status' => 'pending',
            'type' => 'mas-ahnasa'
        ];
        return $fills;
    }

    /**
     * @param Client $client
     * @return array
     */
    private function createBituachLeumiReport(Client $client)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('bituach-leumi'),
            'settings' => [],
            'status' => 'pending',
            'type' => 'bituach-leumi'
        ];
        return $fills;
    }

    /**
     * @param Client $client
     * @param $sum
     * @return array
     */
    private function createItraReport(Client $client, $sum)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('itra'),
            'settings' => [
                'sum' => ['data' => $sum, 'title' => 'סכום']
            ],
            'status' => 'pending',
            'type' => 'itra'
        ];
        return $fills;
    }

    /**
     * @param Client $client
     * @return array
     */
    private function createTipulimShonimReport(Client $client)
    {
        $fills = [
            'client_id' => $client->id,
            'name' => $this->nameRep('tipulim-shonim'),
            'settings' => [],
            'status' => 'pending',
            'type' => 'tipulim-shonim'
        ];
        return $fills;
    }

    private function createCustomReport($client, $name, $cr_id)
    {
        $fills = [
            'client_id' => $client->id,
            'cr_id' => $cr_id,
            'name' => $name,
            'settings' => [],
            'status' => 'pending',
            'type' => 'custom'
        ];
        return $fills;
    }

    public function fillReport(Client $client, $type, $settings = [])
    {
        switch ($type) {
            case 'annual-report':
                if (!array_key_exists('year', $settings) || is_null($settings['year'])) {
                    throw new TooFewArgumentsToReportException('Too few arguments to report: annual-report. Expected: Year.');
                }
                return $this->fill($this->createAnnualReport($client, $settings['year']));

            case 'declaration-of-capital':
                if (!array_key_exists('date', $settings) || is_null($settings['date'])) {
                    throw new TooFewArgumentsToReportException('Too few arguments to report: declaration-of-capital. Expected: Date.');
                }
                return $this->fill($this->createDeclarationOfCapitalReport($client, $settings['date']));

            case 'ishur-nikui-mas-bamakor':
                if (!array_key_exists('date', $settings) || is_null($settings['date']) || !array_key_exists('percents', $settings) || is_null($settings['percents'])) {
                    throw new TooFewArgumentsToReportException('Too few arguments to report: ishur-nikui-mas-bamakor. Expected: Date, Percents.');
                }
                return $this->fill($this->createIshurNikuiMasBamakorReport($client, $settings['date'], $settings['percents']));

            case 'mam':
                return $this->fill($this->createMamReport($client));

            case 'mas-ahnasa':
                return $this->fill($this->createMasAhnasaReport($client));

            case 'bituach-leumi':
                return $this->fill($this->createBituachLeumiReport($client));

            case 'itra':
                if (!array_key_exists('sum', $settings) || is_null($settings['sum'])) {
                    throw new TooFewArgumentsToReportException('Too few arguments to report: itra. Expected: Sum.');
                }
                return $this->fill($this->createItraReport($client, $settings['sum']));

            case 'tipulim-shonim':
                return $this->fill($this->createTipulimShonimReport($client));
            case 'custom':
                if (!array_key_exists('name', $settings) || is_null($settings['name']) || !array_key_exists('cr_id', $settings) || is_null($settings['cr_id'])) {
                    throw new TooFewArgumentsToReportException('Too few arguments to report: custom. Expected: Name, cr_id.');
                }
                $this->fill($this->createCustomReport($client, $settings['name'], $settings['cr_id']));
                return $this;
        }
    }

    /**
     * @param null $comment
     */
    public function addComment($comment = null)
    {
        (new Comment())->fill(['parent_id' => $this->id, 'comment' => $comment])->save();
    }

    public function complete()
    {
        return $this->update(['status' => 'complete']);
    }
}