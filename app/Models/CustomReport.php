<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomReports
 *
 * @property int $id
 * @property int $user_id
 * @property int $client_id
 * @property int $report_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Client $client
 * @property-read \App\Models\Report $report
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomReports whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomReports whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomReports whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomReports whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomReports whereReportId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomReports whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CustomReports whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Report[] $reports
 */
class CustomReport extends Model
{
    protected $fillable = [
        'user_id', 'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reports()
    {
        return $this->hasMany(Report::class, 'cr_id');
    }

    /**
     * @return Report
     */
    public function pendingReports()
    {
        return $this->reports()->where('status', 'pending');
    }

    public function reportByClient($client_id)
    {
        return $this->pendingReports()->where('client_id', $client_id)->first();
    }
}
