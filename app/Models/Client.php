<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property int $user_id
 * @property string $number
 * @property string $name
 * @property string $passport
 * @property string $tiknikuim
 * @property string $phone
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client wherePassport($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereTiknikuim($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Client whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Report[] $reports
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CustomReports[] $customReports
 */
class Client extends Model
{
    protected $fillable = [
        'number',
        'name',
        'passport',
        'tiknikuim',
        'phone',
        'email'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany(Report::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function comments()
    {
        return $this->hasManyThrough(Comment::class, Report::class, 'client_id', 'parent_id');
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getPendingReport($type)
    {
        $builder = $this->reports()->where('type', $type)->where('status', 'pending');
        return $builder;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function reportExists($type)
    {
        $report = $this->getPendingReport($type)->first();
        return $report;
    }

    /**
     * @return mixed
     */
    public function annualReports()
    {
        return $this->reports()->where('type', 'annual-report');
    }

    /**
     * @return mixed
     */
    public function declarationOfCapitalReports()
    {
        return $this->reports()->where('type', 'declaration-of-capital');
    }

    /**
     * @return mixed
     */
    public function ishurNikuiMasBamakorReports()
    {
        return $this->reports()->where('type', 'ishur-nikui-mas-bamakor');
    }

    /**
     * @return mixed
     */
    public function mamReports()
    {
        return $this->reports()->where('type', 'mam');
    }

    /**
     * @return mixed
     */
    public function masAhnasaReports()
    {
        return $this->reports()->where('type', 'mas-ahnasa');
    }

    /**
     * @return mixed
     */
    public function bituachLeumiReports()
    {
        return $this->reports()->where('type', 'bituach-leumi');
    }

    /**
     * @return mixed
     */
    public function itraReports()
    {
        return $this->reports('type', 'itra')->where();
    }

    /**
     * @return mixed
     */
    public function tipulimShonimReports()
    {
        return $this->reports()->where('type', 'tipulim-shonim');
    }
}