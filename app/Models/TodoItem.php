<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TodoItem
 *
 * @property int $id
 * @property int $report_id
 * @property string $name
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Report $report
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TodoItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TodoItem whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TodoItem whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TodoItem whereReportId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TodoItem whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TodoItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TodoItem extends Model
{
    protected $fillable = [
        'user_id', 'report_id', 'name', 'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report()
    {
        return $this->belongsTo(Report::class, 'report_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return bool
     */
    public function complete()
    {
        return $this->update(['status' => 'complete']);
    }

}
