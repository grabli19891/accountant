@extends('index')

@section('title', 'Main')

@section('index')
    <p class="center-align">
        @unless(Sentinel::check())
            <a class="btn" href="{{ route('login') }}">@lang('index.sign_in')</a>
            <a class="btn" href="{{ route('registration.create') }}">@lang('index.sign_up')</a>
        @endunless

        @if(Sentinel::check())
            <a class="btn" href="{{ route('logout') }}">@lang('index.logout')</a>
            <a class="btn" href="{{ route('dashboard.index') }}">@lang('index.dashboard')</a>
        @endif
        @if(Sentinel::check() && Sentinel::hasAccess('admin'))
            <a class="btn" href="{{ route('admin.index') }}">@lang('index.admin_panel')</a>
        @endif
    </p>
@endsection