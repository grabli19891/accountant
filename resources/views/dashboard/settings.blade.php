@extends('dashboard.main')

@section('title', 'Settings')

@section('index')
    <h1>@lang('dashboard/settings.settings')</h1>
    <div class="row">
        <div class="col s12">
            <ul class="tabs" dir="ltr">
                <li class="tab col s3"><a class="{{ isActiveTab('profile') }} settings_tab" href="#profile">@lang('dashboard/settings.profile')</a></li>
                <li class="tab col s3"><a class="{{ isActiveTab('add_custom_report') }} settings_tab" href="#add_custom_report">@lang('dashboard/settings.add_custom_report')</a></li>
                <li class="tab col s3"><a class="{{ isActiveTab('custom_reports') }} settings_tab" href="#custom_reports">@lang('dashboard/settings.custom_reports')</a></li>
            </ul>
        </div>
        <div id="custom_reports" class="col s12">
            @include('dashboard.tabs.custom_reports')
        </div>
        <div id="add_custom_report" class="col s12">
            @include('dashboard.tabs.add_custom_report')
        </div>
        <div id="profile" class="col s12">
            @include('dashboard._common._profile_form')
        </div>
    </div>

@endsection