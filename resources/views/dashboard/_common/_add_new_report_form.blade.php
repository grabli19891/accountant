{{ Form::open(['route' => 'reports.createCustomReport', 'method' => 'POST']) }}
<div class="row">
    <div class="col s12 m6 offset-m3 center">
        <div class="input-field">
            <input name="name" id="custom-report-name" type="text">
            <label for="custom-report-name">@lang('dashboard/settings.type_name')</label>
        </div>
        <a id="addCustomReport" onclick="this.parentNode.parentNode.parentNode.submit(); return false;" class="btn waves-effect waves-light"><i class="material-icons">add</i></a>
    </div>
</div>
{{ Form::close() }}