@extends('layouts.dashboard')

@section('title', 'Dashboard')

@section('dashboard')
        <div>
            <ul id="user_menu" class="dropdown-content">
                <li><a href="{{ route('dashboard.settings') }}">@lang('dashboard/navbar.settings')</a></li>
                <li><a href="{{ route('logout') }}">@lang('dashboard/navbar.logout')</a></li>
            </ul>
            <nav>
                <div class="nav-wrapper teal">
                    <div class="container">
                        <a href="#" data-activates="mobile" class="button-collapse right" style="right: 11px;"><i class="material-icons">menu</i></a>
                        <ul id="nav-mobile" class="hide-on-med-and-down right">
                            <li class="{{ isActiveRoute('reports.index') }}"><a href="{{ route('reports.index') }}">@lang('dashboard/navbar.report')</a></li>
                            <li class="{{ isActiveRoute('clients.index') }}"><a href="{{ route('clients.index') }}">@lang('dashboard/navbar.clients')</a></li>
                            <li class="{{ isActiveRoute('dashboard.index') }}"><a href="{{ route('dashboard.index') }}">@lang('dashboard/navbar.dashboard')</a></li>
                        </ul>
                        <ul class="left">
                            <li><a class="dropdown-button" href="#!" data-activates="user_menu">{{ Sentinel::getUser()->email }}<i class="material-icons right">arrow_drop_down</i></a></li>
                        </ul>
                        <ul class="side-nav" id="mobile">
                            <li class="{{ isActiveRoute('dashboard.index') }}"><a href="{{ route('dashboard.index') }}">@lang('dashboard/navbar.dashboard')</a></li>
                            <li class="{{ isActiveRoute('clients.index') }}"><a href="{{ route('clients.index') }}">@lang('dashboard/navbar.clients')</a></li>
                            <li class="{{ isActiveRoute('reports.index') }}"><a href="{{ route('reports.index') }}">@lang('dashboard/navbar.report')</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <main>
            <div class="container">
                @yield('index')
            </div>
        </main>

        {{--<footer class="page-footer teal">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col l6 s12">--}}
                        {{--<h5 class="white-text">Footer Content</h5>--}}
                        {{--<p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>--}}
                    {{--</div>--}}
                    {{--<div class="col l4 offset-l2 s12">--}}
                        {{--<h5 class="white-text">Links</h5>--}}
                        {{--<ul>--}}
                            {{--<li><a class="grey-text text-lighten-3" href="{{ route('dashboard.index') }}">@lang('dashboard/navbar.dashboard')</a></li>--}}
                            {{--<li><a class="grey-text text-lighten-3" href="{{ route('clients.index') }}">@lang('dashboard/navbar.clients')</a></li>--}}
                            {{--<li><a class="grey-text text-lighten-3" href="{{ route('reports.index') }}">@lang('dashboard/navbar.report')</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="footer-copyright">--}}
                {{--<div class="container">--}}
                    {{--© 2014 Copyright Text--}}
                    {{--<a class="grey-text text-lighten-4 right" href="#!">More Links</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</footer>--}}
@endsection