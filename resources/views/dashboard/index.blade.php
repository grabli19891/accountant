@extends('dashboard.main')

@section('index')

    <h1>@lang('dashboard/index.dashboard')</h1>

    <div class="row">
        <div class="input-field col s12 inline-block">
            {{ Form::select('clients[]', array_merge(['all' => __('dashboard/index.all')], $clients->pluck('name', 'name')->all()), null, ['id' => 'clients_filter', 'class' => 'clients_filter']) }}
            <label>@lang('dashboard/index.select_client')</label>
        </div>
        <div class="col s12">
            <table id="reports" class="compact bordered highlight" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="right-align">@lang('dashboard/index.client')</th>
                    <th class="right-align">@lang('dashboard/index.name')</th>
                    <th class="right-align">@lang('dashboard/index.settings')</th>
                    {{--<th>@lang('dashboard/index.status')</th>--}}
                    <th class="right-align">@lang('dashboard/index.actions')</th>
                </tr>
                </thead>

                <tfoot>
                <tr class="filter">
                    <th class="right-align"></th>
                    <th class="right-align"></th>
                    <th class="right-align"></th>
                    {{--<th>@lang('dashboard/index.status')</th>--}}
                    <th class="right-align"></th>
                </tr>
                </tfoot>

                <tbody>
                @foreach($reports as $report)
                    <tr>
                        <td class="right-align"><a href="{{ route('clients.show', $report->client->id) }}">{{ $report->client->name }}</a></td>
                        <td class="right-align">{{ $report->name }}</td>
                        <td class="right-align">
                            <ul>
                                @foreach($report->settings as $key => $value)
                                    <li><b>{{ $value['title'] }}</b>: {{ $value['data'] }}</li>
                                @endforeach
                            </ul>
                        </td>
                        {{--<td class="report_status">{{ $report->status }}</td>--}}
                        <td class="right-align">
                            @if($report->status === 'pending')
                                <a href="#!" data-report_id="{{ $report->id }}" data-type="{{ $report->type }}" data-client_id="{{ $report->client->id }}" class="complete-button">@lang('dashboard/index.complete')</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>

        </div>
    </div>
    <script>
        var report_completed = '{{ __('dashboard/index.report_completed') }}';
    </script>

@endsection