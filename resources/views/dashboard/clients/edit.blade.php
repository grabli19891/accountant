@extends('dashboard.main')

@section('title', 'Edit client')

    @section('index')
        @include('dashboard.clients._partials._client_edit_form')
    @endsection