@extends('dashboard.main')

@section('title', 'Create client')

    @section('index')
        @include('dashboard.clients._partials._client_create_form')
    @endsection