{{ Form::model($client, ['route' => ['clients.update', $client->id], 'method' => 'PUT']) }}

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('number', null, ['id' => 'number', 'class' => $errors->has('number') ? 'validate invalid' : 'validate ']) }}
        <label for="number" data-error="{{ $errors->first('number') }}">@lang('dashboard/clients/form.number')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('name', null, ['id' => 'name', 'class' => $errors->has('name') ? 'validate invalid' : 'validate ']) }}
        <label for="name" data-error="{{ $errors->first('name') }}">@lang('dashboard/clients/form.name')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('passport', null, ['id' => 'passport', 'class' => $errors->has('passport') ? 'validate invalid' : 'validate ']) }}
        <label for="passport" data-error="{{ $errors->first('passport') }}">@lang('dashboard/clients/form.passport')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('tiknikuim', null, ['id' => 'tiknikuim', 'class' => $errors->has('tiknikuim') ? 'validate invalid' : 'validate ']) }}
        <label for="tiknikuim" data-error="{{ $errors->first('tiknikuim') }}">@lang('dashboard/clients/form.tiknikuim')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('phone', null, ['id' => 'phone', 'class' => $errors->has('phone') ? 'validate invalid phone' : 'validate ']) }}
        <label for="phone" data-error="{{ $errors->first('phone') }}">@lang('dashboard/clients/form.phone')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::email('email', null, ['id' => 'email', 'class' => $errors->has('email') ? 'validate invalid' : 'validate ']) }}
        <label for="email" data-error="{{ $errors->first('email') }}">@lang('dashboard/clients/form.email')</label>
    </div>
</div>

<div class="row">
    <div class="col s6 center">
        <button class="btn waves-effect waves-light blue darken-2" type="submit" name="action">@lang('dashboard/clients/form.submit')
            <i class="material-icons right">send</i>
        </button></div>
    <div class="col s6 center">
        <button class="btn waves-effect waves-light blue darken-2" type="reset" name="action">@lang('dashboard/clients/form.reset')
            <i class="material-icons right">undo</i>
        </button>
    </div>
</div>
{{ Form::close() }}