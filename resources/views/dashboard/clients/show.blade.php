@extends('dashboard.main')

@section('title', 'Show client')

@section('index')

    <div class="row">
        <div class="col s12 m6">
            <div class="card">
                <div class="card-content">
                    <span class="card-title center-align">{{ $client->name }}</span>
                    <div class="row">
                        <div class="col s3 right-align">@lang('dashboard/clients/show.number')</div>
                        <div class="col s9">{{ $client->number }}</div>
                    </div>
                    <div class="row">
                        <div class="col s3 right-align">@lang('dashboard/clients/show.passport')</div>
                        <div class="col s9">{{ $client->passport }}</div>
                    </div>
                    <div class="row">
                        <div class="col s3 right-align">@lang('dashboard/clients/show.tiknikuim')</div>
                        <div class="col s9">{{ $client->tiknikuim }}</div>
                    </div>
                    <div class="row">
                        <div class="col s3 right-align">@lang('dashboard/clients/show.phone')</div>
                        <div class="col s9">{{ $client->phone }}</div>
                    </div>
                    <div class="row">
                        <div class="col s3 right-align">@lang('dashboard/clients/show.email')</div>
                        <div class="col s9">{{ $client->email }}</div>
                    </div>
                </div>
                <div class="card-action">
                    <a href="{{ route('clients.edit', $client->id) }}">@lang('dashboard/clients/show.edit_client')</a>
                    {{ Form::open(['route' => ['clients.destroy', $client->id], 'method' => 'DELETE', 'style' => 'display: inline;']) }}
                    <a href="#" onclick="this.parentNode.submit(); return false;">@lang('dashboard/clients/show.drop_client')</a>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <div class="card" style="overflow: scroll; overflow: visible;">
                <div class="card-content" style="padding: 5px;">
                    <span class="card-title center-align">@lang('dashboard/clients/show.reports')</span>
                    {{ Form::open() }}
                    {{ Form::hidden('client_id', $client->id) }}

                    <ul class="collapsible report-list" data-collapsible="expandable">
                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.annual-report')</div>
                            <div class="collapsible-body" style="left: 0">

                                {{--Annual Report--}}
                                @php
                                    $report = $client->reportExists('annual-report');
                                @endphp
                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.annual-report')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('annualReportSwitch', 'annualReportSwitch', $report ? true : false, ['id' => 'annualReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        <div class="input-field col s12">
                                            {{ Form::select('annualReportYear', [], $report ? $report->settings['year']['data'] : null, ['id' => 'annualReportYear', 'disabled' => 'disabled', 'placeholder' => __('dashboard/clients/show.select_year')]) }}
                                            <label>@lang('dashboard/clients/show.select_year')</label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'annualReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>

                                </div>

                                <div class="row">
                                        <div class="comments {{ !$report ? 'hide' : '' }}">
                                            @if($report)
                                                {{ Form::hidden('report_id', $report->id) }}
                                            @else
                                                {{ Form::hidden('report_id', 0) }}
                                            @endif
                                            <p>@lang('dashboard/clients/show.comments'):</p>
                                            <ul class="comments_list collection">
                                                @if($report)
                                                @foreach($report->comments as $comment)
                                                    <li class="comment collection-item">
                                                        <div>
                                                            {{ $comment->comment }}
                                                            <a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                                @endif
                                            </ul>

                                            <div class="input-field col s12">
                                                <input id="annual-report-comment" type="text">
                                                <label for="annual-report-comment">@lang('dashboard/clients/show.annual-report') @lang('dashboard/clients/show.comment')</label>
                                            </div>
                                            <a href="#!" class="add-comment-link" id="add-comment-link">@lang('dashboard/clients/show.add_comment')</a>
                                            <a href="#!" class="hide btn waves-effect waves-green append-comment" id="append-comment">@lang('dashboard/clients/show.add')</a>
                                        </div>
                                </div>
                            </div>
                        </li>
                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.declaration-of-capital')</div>
                            <div class="collapsible-body">

                                {{--Declaration of capital--}}
                                @php
                                    $report = $client->reportExists('declaration-of-capital');
                                @endphp
                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.declaration-of-capital')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('declarationOfCapitalReportSwitch', 'declarationOfCapitalReportSwitch', $report ? true : false, ['id' => 'declarationOfCapitalReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        {{ Form::text(
                                                    'declarationOfCapitalReportDate', $report ? $report->settings['date']['data'] : '',
                                                    ['id' => 'declarationOfCapitalReportDate', 'class' => 'datepicker', 'disabled' => true]
                                                ) }}
                                        <label for="declarationOfCapitalReportDate">@lang('dashboard/clients/show.select_date')</label>
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'declarationOfCapitalReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="comments {{ !$report ? 'hide' : '' }}">
                                        @if($report)
                                            {{ Form::hidden('report_id', $report->id) }}
                                        @else
                                            {{ Form::hidden('report_id', 0) }}
                                        @endif
                                            <p>@lang('dashboard/clients/show.comments'):</p>
                                            <ul class="comments_list collection">
                                            @if($report)
                                                @foreach($report->comments as $comment)
                                                    <li class="comment collection-item">
                                                        <div>
                                                            {{ $comment->comment }}
                                                            <a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                        <div class="input-field col s12">
                                            <input id="declaration-of-capital-comment" type="text">
                                            <label for="declaration-of-capital-comment">@lang('dashboard/clients/show.declaration-of-capital') @lang('dashboard/clients/show.comment')</label>
                                        </div>
                                        <a href="#!" class="add-comment-link" id="add-comment-link">@lang('dashboard/clients/show.add_comment')</a>
                                        <a href="#!" class="hide btn waves-effect waves-green append-comment" id="append-comment">@lang('dashboard/clients/show.add')</a>
                                    </div>
                                </div>

                            </div>
                        </li>
                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.ishur-nikui-mas-bamakor')</div>
                            <div class="collapsible-body">

                                {{--Ishur nikui mas bamakor--}}

                                @php
                                    $report = $client->reportExists('ishur-nikui-mas-bamakor');
                                @endphp

                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.ishur-nikui-mas-bamakor')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('IshurNikuiMasBamakorReportSwitch', 'IshurNikuiMasBamakorReportSwitch', $report ? true : false, ['id' => 'IshurNikuiMasBamakorReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        @php
                                            if($report) {
                                                $date = \Carbon\Carbon::createFromFormat('j F, Y', $report->settings['date']['data']);
                                                $alert = \Carbon\Carbon::now()->diffInDays($date) <= 7;
                                            }
                                        @endphp
                                        <span id="percents">
                                            <div class="input-field inline">
                                                {{--<input id="ishur-nikui-mas-bamakor-pecents" type="number">--}}
                                                {{ Form::number('ishur-nikui-mas-bamakor-pecents', $report ? $report->settings['percents']['data'] : null, ['id' => 'ishur-nikui-mas-bamakor-pecents', 'disabled' => true]) }}
                                                <label for="ishur-nikui-mas-bamakor-pecents">@lang('dashboard/clients/show.percents')</label>
                                            </div>
                                        </span>
                                        {{ Form::text(
                                                        'IshurNikuiMasBamakorReportDate',
                                                        $report ? $report->settings['date']['data'] : '',
                                                        ['id' => 'IshurNikuiMasBamakorReportDate', 'class' => 'datepicker', 'disabled' => true]
                                                    ) }}
                                        <label for="IshurNikuiMasBamakorReportDate">@lang('dashboard/clients/show.select_date')</label>
                                        <span class="date-alert red-text {{ !isset($alert) || !$alert ? ' hide': '' }}">@lang('dashboard/clients/show.date_alert')</span>
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'IshurNikuiMasBamakorReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="comments {{ !$report ? 'hide' : '' }}">
                                        @if($report)
                                            {{ Form::hidden('report_id', $report->id) }}
                                        @else
                                            {{ Form::hidden('report_id', 0) }}
                                        @endif
                                        <p>@lang('dashboard/clients/show.comments'):</p>
                                        <ul class="comments_list collection">
                                            @if($report)
                                                @foreach($report->comments as $comment)
                                                    <li class="comment collection-item">
                                                        <div>
                                                            {{ $comment->comment }}
                                                            <a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                        <div class="input-field col s12">
                                            <input id="ishur-nikui-mas-bamakor-comment" type="text">
                                            <label for="ishur-nikui-mas-bamakor-comment">@lang('dashboard/clients/show.ishur-nikui-mas-bamakor') @lang('dashboard/clients/show.comment')</label>
                                        </div>
                                        <a href="#!" class="add-comment-link" id="add-comment-link">@lang('dashboard/clients/show.add_comment')</a>
                                        <a href="#!" class="hide btn waves-effect waves-green append-comment" id="append-comment">@lang('dashboard/clients/show.add')</a>
                                    </div>
                                </div>

                            </div>
                        </li>

                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.mam')</div>
                            <div class="collapsible-body">

                                {{--Mam--}}
                                @php
                                    $report = $client->reportExists('mam');
                                @endphp

                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.mam')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('MamReportSwitch', 'MamReportSwitch', $report ? true : false, ['id' => 'MamReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        &nbsp;
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'MamReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>

                                </div>

                                {{--<div class="row">--}}
                                    {{--<div class="comments {{ !$report ? 'hide' : '' }}">--}}
                                        {{--@if($report)--}}
                                            {{--{{ Form::hidden('report_id', $report->id) }}--}}
                                        {{--@else--}}
                                            {{--{{ Form::hidden('report_id', 0) }}--}}
                                        {{--@endif--}}
                                        {{--<ul class="comments_list collection">--}}
                                            {{--@if($report)--}}
                                                {{--@foreach($report->comments as $comment)--}}
                                                    {{--<li class="comment collection-item">--}}
                                                        {{--<div>--}}
                                                            {{--{{ $comment->comment }}--}}
                                                            {{--<a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>--}}
                                                        {{--</div>--}}
                                                    {{--</li>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</ul>--}}

                                        {{--<div class="input-field col s12">--}}
                                            {{--<input id="mam-comment" type="text">--}}
                                            {{--<label for="mam-comment">Mam comment</label>--}}
                                        {{--</div>--}}
                                        {{--<a href="#!" class="add-comment-link" id="add-comment-link">Add comment...</a>--}}
                                        {{--<a href="#!" class="hide append-comment" id="append-comment">Add</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="row">
                                    <div class="todos {{ !$report ? 'hide' : '' }}">
                                        @if($report)
                                            {{ Form::hidden('report_id', $report->id) }}
                                        @else
                                            {{ Form::hidden('report_id', 0) }}
                                        @endif
                                        <ul class="todos_list collection">
                                            @if($report)
                                                @foreach($report->todoItems as $item)
                                                    <li class="todo collection-item">
                                                        <div>
                                                            <span class="title"><b>@lang('dashboard/clients/show.Todo'):</b> {{ $item->name }} <b>/@lang('dashboard/clients/show.status'):</b> {{ $item->status }}</span>
                                                            <a href="#!" class="secondary-content delete-todo" data-todo_id="{{ $item->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                        <div class="input-field col s12">
                                            <input id="mam-todo" type="text">
                                            <label for="mam-todo">@lang('dashboard/clients/show.mam') @lang('dashboard/clients/show.todo')</label>
                                        </div>
                                        <a href="#!" class="add-todo-link" id="add-todo-link">@lang('dashboard/clients/show.add_todo')</a>
                                        <a href="#!" class="hide btn waves-effect waves-green append-todo" id="append-todo">@lang('dashboard/clients/show.add')</a>
                                    </div>
                                </div>

                            </div>
                        </li>

                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.mas-ahnasa')</div>
                            <div class="collapsible-body">

                                {{--Mas ahnasa--}}
                                @php
                                    $report = $client->reportExists('mas-ahnasa');
                                @endphp

                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.mas-ahnasa')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('MasAhnasaReportSwitch', 'MasAhnasaReportSwitch', $report ? true : false, ['id' => 'MasAhnasaReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        &nbsp;
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'MasAhnasaReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>

                                </div>

                                {{--<div class="row">--}}
                                    {{--<div class="comments {{ !$report ? 'hide' : '' }}">--}}
                                        {{--@if($report)--}}
                                            {{--{{ Form::hidden('report_id', $report->id) }}--}}
                                        {{--@else--}}
                                            {{--{{ Form::hidden('report_id', 0) }}--}}
                                        {{--@endif--}}
                                        {{--<ul class="comments_list collection">--}}
                                            {{--@if($report)--}}
                                                {{--@foreach($report->comments as $comment)--}}
                                                    {{--<li class="comment collection-item">--}}
                                                        {{--<div>--}}
                                                            {{--{{ $comment->comment }}--}}
                                                            {{--<a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>--}}
                                                        {{--</div>--}}
                                                    {{--</li>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</ul>--}}

                                        {{--<div class="input-field col s12">--}}
                                            {{--<input id="mas-ahnasa-comment" type="text">--}}
                                            {{--<label for="mas-ahnasa-comment">Mas ahnasa comment</label>--}}
                                        {{--</div>--}}
                                        {{--<a href="#!" class="add-comment-link" id="add-comment-link">Add comment...</a>--}}
                                        {{--<a href="#!" class="hide append-comment" id="append-comment">Add</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="row">
                                    <div class="todos {{ !$report ? 'hide' : '' }}">
                                        @if($report)
                                            {{ Form::hidden('report_id', $report->id) }}
                                        @else
                                            {{ Form::hidden('report_id', 0) }}
                                        @endif
                                        <ul class="todos_list collection">
                                            @if($report)
                                                @foreach($report->todoItems as $item)
                                                    <li class="todo collection-item">
                                                        <div>
                                                            <span class="title"><b>@lang('dashboard/clients/show.Todo'):</b> {{ $item->name }} <b>/@lang('dashboard/clients/show.status'):</b> {{ $item->status }}</span>
                                                            <a href="#!" class="secondary-content delete-todo" data-todo_id="{{ $item->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                        <div class="input-field col s12">
                                            <input id="mas-ahnasa-todo" type="text">
                                            <label for="mas-ahnasa-todo">@lang('dashboard/clients/show.mas-ahnasa') @lang('dashboard/clients/show.todo')</label>
                                        </div>
                                        <a href="#!" class="add-todo-link" id="add-todo-link">@lang('dashboard/clients/show.add_todo')</a>
                                        <a href="#!" class="hide btn waves-effect waves-green append-todo" id="append-todo">@lang('dashboard/clients/show.add')</a>
                                    </div>
                                </div>

                            </div>
                        </li>

                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.bituach-leumi')</div>
                            <div class="collapsible-body">

                                {{--Bituah leumi--}}
                                @php
                                    $report = $client->reportExists('bituach-leumi');
                                @endphp

                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.bituach-leumi')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('BituahLeumiReportSwitch', 'BituahLeumiReportSwitch', $report ? true : false, ['id' => 'BituahLeumiReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        &nbsp;
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'BituahLeumiReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>

                                </div>

                                {{--<div class="row">--}}
                                    {{--<div class="comments {{ !$report ? 'hide' : '' }}">--}}
                                        {{--@if($report)--}}
                                            {{--{{ Form::hidden('report_id', $report->id) }}--}}
                                        {{--@else--}}
                                            {{--{{ Form::hidden('report_id', 0) }}--}}
                                        {{--@endif--}}
                                        {{--<ul class="comments_list collection">--}}
                                            {{--@if($report)--}}
                                                {{--@foreach($report->comments as $comment)--}}
                                                    {{--<li class="comment collection-item">--}}
                                                        {{--<div>--}}
                                                            {{--{{ $comment->comment }}--}}
                                                            {{--<a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>--}}
                                                        {{--</div>--}}
                                                    {{--</li>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</ul>--}}

                                        {{--<div class="input-field col s12">--}}
                                            {{--<input id="bituach-leumi-comment" type="text">--}}
                                            {{--<label for="bituach-leumi-comment">Bituah leumi comment</label>--}}
                                        {{--</div>--}}
                                        {{--<a href="#!" class="add-comment-link" id="add-comment-link">Add comment...</a>--}}
                                        {{--<a href="#!" class="hide append-comment" id="append-comment">Add</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="row">
                                    <div class="todos {{ !$report ? 'hide' : '' }}">
                                        @if($report)
                                            {{ Form::hidden('report_id', $report->id) }}
                                        @else
                                            {{ Form::hidden('report_id', 0) }}
                                        @endif
                                        <ul class="todos_list collection">
                                            @if($report)
                                                @foreach($report->todoItems as $item)
                                                    <li class="todo collection-item">
                                                        <div>
                                                            <span class="title"><b>@lang('dashboard/clients/show.Todo'):</b> {{ $item->name }} <b>/@lang('dashboard/clients/show.status'):</b> {{ $item->status }}</span>
                                                            <a href="#!" class="secondary-content delete-todo" data-todo_id="{{ $item->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                        <div class="input-field col s12">
                                            <input id="bituach-leumi-todo" type="text">
                                            <label for="bituach-leumi-todo">@lang('dashboard/clients/show.bituach-leumi') @lang('dashboard/clients/show.todo')</label>
                                        </div>
                                        <a href="#!" class="add-todo-link" id="add-todo-link">@lang('dashboard/clients/show.add_todo')</a>
                                        <a href="#!" class="hide btn waves-effect waves-green append-todo" id="append-todo">@lang('dashboard/clients/show.add')</a>
                                    </div>
                                </div>

                            </div>
                        </li>

                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.itra')</div>
                            <div class="collapsible-body">

                                {{--Itra--}}
                                @php
                                    $report = $client->reportExists('itra');
                                @endphp

                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.itra')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('ItraReportSwitch', 'ItraReportSwitch', $report ? true : false, ['id' => 'ItraReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        <div class="input-field inline">
                                            {{ Form::number('itra-sum', $report ? $report->settings['sum']['data'] : null, ['id' => 'itra-sum', 'disabled' => true]) }}
                                            <label for="itra-sum">@lang('dashboard/clients/show.itra')</label>
                                        </div>
                                        <a href="#!" id="itra-activate" class="btn waves-effect waves-green disabled">@lang('dashboard/clients/show.activate')</a>
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'ItraReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="comments {{ !$report ? 'hide' : '' }}">
                                        @if($report)
                                            {{ Form::hidden('report_id', $report->id) }}
                                        @else
                                            {{ Form::hidden('report_id', 0) }}
                                        @endif
                                        <p>@lang('dashboard/clients/show.comments')</p>
                                        <ul class="comments_list collection">
                                            @if($report)
                                                @foreach($report->comments as $comment)
                                                    <li class="comment collection-item">
                                                        <div>
                                                            {{ $comment->comment }}
                                                            <a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                        <div class="input-field col s12">
                                            <input id="itra-comment" type="text">
                                            <label for="itra-comment">@lang('dashboard/clients/show.itra') @lang('dashboard/clients/show.comment')</label>
                                        </div>
                                        <a href="#!" class="add-comment-link" id="add-comment-link">@lang('dashboard/clients/show.add_comment')</a>
                                        <a href="#!" class="hide btn waves-effect waves-green append-comment" id="append-comment">@lang('dashboard/clients/show.add')</a>
                                    </div>
                                </div>

                            </div>
                        </li>

                        <li class="report-node">
                            <div class="collapsible-header">@lang('dashboard/clients/show.tipulim-shonim')</div>
                            <div class="collapsible-body">

                                {{--Tipulim shonim--}}
                                @php
                                    $report = $client->reportExists('tipulim-shonim');
                                @endphp

                                <div class="row">

                                    <div class="col s12 center-align">
                                        <h5>@lang('dashboard/clients/show.tipulim-shonim')</h5>
                                    </div>
                                    <div class="col s4">
                                        <div class="switch center">
                                            <label>
                                                {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                {{--@lang('dashboard/clients/show.off')--}}
                                                {{ Form::checkbox('TipulimShonimReportSwitch', 'TipulimShonimReportSwitch', $report ? true : false, ['id' => 'TipulimShonimReportSwitch', 'disabled' => $report ? true : false, 'style' => 'left:0;']) }}
                                                <span class="lever"></span>
                                                {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col s4">
                                        &nbsp;
                                    </div>
                                    <div class="col s4">
                                        {{ Form::button('<i class="material-icons">done</i>', ['id' => 'TipulimShonimReportComplete', 'class' => 'btn-floating waves-effect waves-green center', 'disabled' => !$report ? true : false]) }}
                                    </div>

                                </div>

                                {{--<div class="row">--}}
                                    {{--<div class="comments {{ !$report ? 'hide' : '' }}">--}}
                                        {{--@if($report)--}}
                                            {{--{{ Form::hidden('report_id', $report->id) }}--}}
                                        {{--@else--}}
                                            {{--{{ Form::hidden('report_id', 0) }}--}}
                                        {{--@endif--}}
                                        {{--<ul class="comments_list collection">--}}
                                            {{--@if($report)--}}
                                                {{--@foreach($report->comments as $comment)--}}
                                                    {{--<li class="comment collection-item">--}}
                                                        {{--<div>--}}
                                                            {{--{{ $comment->comment }}--}}
                                                            {{--<a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>--}}
                                                        {{--</div>--}}
                                                    {{--</li>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                        {{--</ul>--}}

                                        {{--<div class="input-field col s12">--}}
                                            {{--<input id="tipulim-shonim-comment" type="text">--}}
                                            {{--<label for="tipulim-shonim-comment">Tipulim shonim comment</label>--}}
                                        {{--</div>--}}
                                        {{--<a href="#!" class="add-comment-link" id="add-comment-link">Add comment...</a>--}}
                                        {{--<a href="#!" class="hide append-comment" id="append-comment">Add</a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="row">
                                    <div class="todos {{ !$report ? 'hide' : '' }}">
                                        @if($report)
                                            {{ Form::hidden('report_id', $report->id) }}
                                        @else
                                            {{ Form::hidden('report_id', 0) }}
                                        @endif
                                        <ul class="todos_list collection">
                                            @if($report)
                                                @foreach($report->todoItems as $item)
                                                    <li class="todo collection-item">
                                                        <div>
                                                            <span class="title"><b>@lang('dashboard/clients/show.Todo'):</b> {{ $item->name }} <b>/@lang('dashboard/clients/show.status'):</b> {{ $item->status }}</span>
                                                            <a href="#!" class="secondary-content delete-todo" data-todo_id="{{ $item->id }}"><i class="material-icons">delete</i></a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>

                                        <div class="input-field col s12">
                                            <input id="tipulim-shonim-todo" type="text">
                                            <label for="tipulim-shonim-todo">@lang('dashboard/clients/show.tipulim-shonim') @lang('dashboard/clients/show.todo')</label>
                                        </div>
                                        <a href="#!" class="add-todo-link" id="add-todo-link">@lang('dashboard/clients/show.add_todo')</a>
                                        <a href="#!" class="hide btn waves-effect waves-green append-todo" id="append-todo">@lang('dashboard/clients/show.add')</a>
                                    </div>
                                </div>

                            </div>
                        </li>

                        @foreach($user->customReports as $cr)
                            @php
                                $report = $cr->reportByClient($client->id);
                            @endphp
                            <li class="report-node">
                                {{ Form::hidden('cr_id', $cr->id) }}
                                <div class="collapsible-header">{{ $cr->name  }}</div>

                                <div class="collapsible-body">

                                    <div class="row">

                                        <div class="col s12 center-align">
                                            <h5 class="caption-report">{{ $cr->name }}</h5>
                                        </div>
                                        <div class="col s4">
                                            <div class="switch center">
                                                <label>
                                                    {{ config('app.locale') === 'he' ? __('dashboard/clients/show.on') : '' }}
                                                    {{--@lang('dashboard/clients/show.off')--}}
                                                    {{ Form::checkbox($cr->id, $cr->id, $report ? true : false, ['disabled' => $report ? true : false, 'class' => 'custom-report-switch', 'style' => 'left:0;']) }}
                                                    <span class="lever"></span>
                                                    {{ config('app.locale') !== 'he' ? __('dashboard/clients/show.on') : '' }}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col s4">
                                            &nbsp;
                                        </div>
                                        <div class="col s4">
                                            {{ Form::button('<i class="material-icons">done</i>', ['class' => 'btn-floating waves-effect waves-green center custom-report-complete', 'disabled' => !$report ? true : false]) }}
                                        </div>

                                    </div>

                                    {{--<div class="row">--}}
                                        {{--<div class="comments {{ !$report ? 'hide' : '' }}">--}}
                                            {{--@if($report)--}}
                                                {{--{{ Form::hidden('report_id', $report->id) }}--}}
                                            {{--@else--}}
                                                {{--{{ Form::hidden('report_id', 0) }}--}}
                                            {{--@endif--}}
                                            {{--<ul class="comments_list collection">--}}
                                                {{--@if($report)--}}
                                                    {{--@foreach($report->comments as $comment)--}}
                                                        {{--<li class="comment collection-item">--}}
                                                            {{--<div>--}}
                                                                {{--{{ $comment->comment }}--}}
                                                                {{--<a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>--}}
                                                            {{--</div>--}}
                                                        {{--</li>--}}
                                                    {{--@endforeach--}}
                                                {{--@endif--}}
                                            {{--</ul>--}}

                                            {{--<div class="input-field col s12">--}}
                                                {{--<input id="custom-comment{{ $cr->id }}" type="text">--}}
                                                {{--<label for="custom-comment{{ $cr->id }}">{{ $cr->name }} comment</label>--}}
                                            {{--</div>--}}
                                            {{--<a href="#!" class="add-comment-link" id="add-comment-link">Add comment...</a>--}}
                                            {{--<a href="#!" class="hide append-comment" id="append-comment">Add</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="row">
                                        <div class="todos {{ !$report ? 'hide' : '' }}">
                                            @if($report)
                                                {{ Form::hidden('report_id', $report->id) }}
                                            @else
                                                {{ Form::hidden('report_id', 0) }}
                                            @endif
                                            <ul class="todos_list collection">
                                                @if($report)
                                                    @foreach($report->todoItems as $item)
                                                        <li class="todo collection-item">
                                                            <div>
                                                                <span class="title"><b>@lang('dashboard/clients/show.Todo'):</b> {{ $item->name }} <b>/@lang('dashboard/clients/show.status'):</b> {{ $item->status }}</span>
                                                                <a href="#!" class="secondary-content delete-todo" data-todo_id="{{ $item->id }}"><i class="material-icons">delete</i></a>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>

                                            <div class="input-field col s12">
                                                <input id="custom-todo{{ $cr->id }}" type="text">
                                                <label for="custom-todo{{ $cr->id }}">{{ $cr->name }} @lang('dashboard/clients/show.todo')</label>
                                            </div>
                                            <a href="#!" class="add-todo-link" id="add-todo-link">@lang('dashboard/clients/show.add_todo')</a>
                                            <a href="#!" class="hide btn waves-effect waves-green append-todo" id="append-todo">@lang('dashboard/clients/show.add')</a>
                                        </div>
                                    </div>

                                </div>
                            </li>

                        @endforeach
                    </ul>
                    <p><a href="{{ route('dashboard.settings', 'add_custom_report') }}">@lang('dashboard/clients/show.add_custom_report')</a></p>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection