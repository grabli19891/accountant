@extends('dashboard.main')

@section('title', 'Clients')

@section('index')

    <h1>@lang('dashboard/clients/index.clients')</h1>
    <a href="{{ route('clients.create') }}" class="waves-effect waves-light btn">@lang('dashboard/clients/index.create')</a>
    <table id="clients" class="compact bordered highlight" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>@lang('dashboard/clients/index.client_number')</th>
            <th class="right-align">@lang('dashboard/clients/index.name')</th>
            <th>@lang('dashboard/clients/index.passport')</th>
            <th>@lang('dashboard/clients/index.tiknikuim')</th>
            <th>@lang('dashboard/clients/index.phone')</th>
            {{--<th>@lang('dashboard/clients/index.email')</th>--}}
            <th>@lang('dashboard/clients/index.actions')</th>
        </tr>
        </thead>

        <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{ $client->number }}</td>
                    <td class="right-align">{{ $client->name }}</td>
                    <td>{{ $client->passport }}</td>
                    <td>{{ $client->tiknikuim }}</td>
                    <td>{{ $client->phone }}</td>
                    {{--<td>{{ $client->email }}</td>--}}
                    <td><a class="client-show" href="{{ route('clients.show', $client->id) }}"><i class="material-icons">pageview</i></a></td>
                </tr>
            @endforeach

        </tbody>

    </table>
@endsection