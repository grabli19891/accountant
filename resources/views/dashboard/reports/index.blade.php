@extends('dashboard.main')

@section('title', 'Reports')

@section('index')

    <h1>
        @if(isset($reports) && $reports->isNotEmpty())
            {{$reports->first()->name}}
        @else
            @lang('dashboard/reports/index.reports')
        @endif
    </h1>

    <div class="row">
    {{Form::open(['method' => 'GET'])}}
        <div class="input-field col s12">
            {{ Form::select('type', $reportsSelect, null, ['id' => 'reports_select', 'placeholder' => __('dashboard/reports/index.select_report'), 'onchange'=>'this.form.submit()']) }}
            <label for="reports_select">@lang('dashboard/reports/index.reports'):</label>
        </div>
    {{Form::close()}}
    </div>

    @if($reports && $reports->isNotEmpty())
        @foreach($reports as $report)
            <div class="card-panel">
                <div class="row">
                    <div class="col s12 m1">
                        <p>@lang('dashboard/reports/index.complete'):</p>
                        {{ Form::button('<i class="material-icons">done</i>', ['class' => 'btn-floating waves-effect waves-green center complete-button-in-reports', 'data-report_id' => $report->id, 'data-type' => $report->type, 'data-client_id' => $report->client->id]) }}
                    </div>
                    <div class="col s12 m2">
                        <p>@lang('dashboard/reports/index.settings'):</p>
                        @if(!empty($report->settings))
                            @foreach($report->settings as $key => $value)
                                <b>{{ $value['title'] }}: </b>{{ $value['data'] }}<br>
                            @endforeach
                        @else
                            <p>@lang('dashboard/reports/index.not_settings')</p>
                        @endif
                    </div>
                    <div class="col s12 m2">
                        <p>@lang('dashboard/reports/index.client_name'):</p>
                        <a href="{{ route('clients.show', $report->client->id) }}">{{ $report->client->name }}</a>
                    </div>
                    @if($report->comments->isNotEmpty())
                        <div class="col s12 m7">
                            <p>@lang('dashboard/reports/index.comments'):</p>
                            <ul class="collection">
                                @foreach($report->comments as $comment)
                                    <li class="collection-item">
                                        <div>
                                            <span class="comment">{{ $comment->comment }}</span>
                                            <a href="#!" class="secondary-content delete-comment" data-comment_id="{{ $comment->id }}"><i class="material-icons">delete</i></a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if($report->todoItems->isNotEmpty())
                        <div class="col s12 m7">
                            <p>@lang('dashboard/reports/index.todo_items'):</p>
                            <ul class="collection">
                                @foreach($report->todoItems as $todoItem)
                                    @if($todoItem->status !== 'complete')
                                    <li class="collection-item">
                                        <div>
                                            <span class="todoName">{{ $todoItem->name }}</span>
                                            <a href="#!" data-todo_id="{{ $todoItem->id }}" class="secondary-content todoCompleteButton"><span class="status">{{ $todoItem->status === 'complete' ? $todoItem->status : __('dashboard/reports/index.carry_out')}}</span><i class="material-icons">done</i></a>
                                        </div>
                                    </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    @else
        <p>
            @if(Request::has('type'))
                <h5 class="center-align">@lang('dashboard/reports/index.select_other_type')</h5>
            @else
                <h5 class="center-align">@lang('dashboard/reports/index.select_type_report')</h5>
            @endif
        </p>
    @endif

@endsection