<table>
    <thead>
    <tr>
        <th>@lang('dashboard/settings.name')</th>
        <th>@lang('dashboard/settings.clients')</th>
        <th>@lang('dashboard/settings.actions')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customReports as $report)
        <tr>
            <td>{{ $report->name }}</td>
            <td>{{ $report->pendingReports()->count() }}</td>
            <td>
                <a href="{{ route('custom-report.delete', $report->id) }}"><i class="material-icons">delete</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>