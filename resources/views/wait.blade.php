@extends('index')

@section('title', 'Please Wait')

@section('index')

<div class="container">

	<div class="center">
		<h4>Please Wait</h4>
	</div>

	<p class="center">
		Very shortly, you will receive an email with instructions on how to continue.
	</p>
	<p class="center">
		<a href="{{ route('default') }}">To main page...</a>
	</p>

</div>

@stop
