<!doctype html>
<html dir="{{ config('app.locale') === 'he' ? 'rtl' : '' }}" lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title') | {{ config('app.name') }}
    </title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">--}}
    <link rel="stylesheet" href="{{ url()->to('/css/materialize.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::to('/css/app.css') }}">


</head>
<body>

{{--<header></header>--}}

@yield('content')
<script src="{{ URL::to('js/app.js') }}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--}}
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="{{ URL::to('js/materialDataTable.js') }}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>--}}
<script src="{{ URL::to('js/lib/materialize.min.js') }}"></script>
{{--TOASTER--}}
@if ($message = Session::get('success'))
    <script>
        Materialize.toast('{{ $message }}', 4000, 'green');
    </script>
@endif

@if ($errors->any())
    @if ($message = $errors->first(0, ':message'))
        <script>
            Materialize.toast('{{ $message }}', 4000, 'red');
        </script>
    @else
        <script>
            Materialize.toast('Please check the form below for errors', 4000);
        </script>
    @endif
@endif

</body>
</html>
