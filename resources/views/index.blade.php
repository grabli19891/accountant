@extends('layouts.main')

    @section('content')

        <div>
            <h1 style="font-weight: lighter" class="center-align"><a href="{{ route('default') }}">{{ config('app.name') }}</a></h1>
            <h3 style="font-weight: lighter" class="center-align">@lang('index.hello'), {{ Sentinel::check() ? Sentinel::getUser()->first_name : 'user'}}</h3>
        </div>

        @yield('index')

    @endsection