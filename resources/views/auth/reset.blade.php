@extends('layouts.main')

@section('title', 'Reset Password')

@section('content')

    <div class="container">

        <div class="center">
            <h4>@lang('auth/reset.reset_password')</h4>
            <div class="divider"></div>
        </div>

        {{ Form::open() }}

        <div class="row">
            <div class="input-field col s12">
                {{ Form::email('email', null, ['class' => 'validate', 'id' => 'email']) }}
                <label for="email" data-error="{{ $errors->first('email') }}">@lang('auth/reset.email')</label>
            </div>
        </div>

        <div class="row">
            <div class="col s12">
                {{ Form::submit(__('auth/reset.reset'), ['class' => 'btn waves-effect waves-light']) }}
            </div>
        </div>

        {{ Form::close() }}
    </div>

@stop