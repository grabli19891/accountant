{{ Form::open() }}

<div class="row">
    <div class="input-field col s12">
        {{ Form::email('email', null, ['id' => 'email', 'class' => $errors->has('email') ? 'validate invalid' : 'validate ']) }}
        <label for="email" data-error="{{ $errors->first('email') }}">@lang('auth/signup.email')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('first_name', null, ['id' => 'first_name', 'class' => $errors->has('first_name') ? 'validate invalid' : 'validate ']) }}
        <label for="first_name" data-error="{{ $errors->first('first_name') }}">@lang('auth/signup.fname')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('last_name', null, ['id' => 'last_name', 'class' => $errors->has('last_name') ? 'validate invalid' : 'validate ']) }}
        <label for="last_name" data-error="{{ $errors->first('last_name') }}">@lang('auth/signup.lname')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('phone', null, ['id' => 'phone', 'class' => $errors->has('phone') ? 'validate invalid phone' : 'validate ']) }}
        <label for="phone" data-error="{{ $errors->first('phone') }}">@lang('auth/signup.phone')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::password('password', ['id' => 'password', 'class' => $errors->has('password') ? 'validate invalid' : ''  ]) }}
        <label for="password" data-error="{{ $errors->first('password') }}">@lang('auth/signup.password')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => $errors->has('password_confirmation') ? 'validate invalid' : '']) }}
        <label for="password_confirmation" data-error="{{ $errors->first('password_confirmation') }}">@lang('auth/signup.confirm_password')</label>
    </div>
</div>

<div class="row">
    <div class="col s6 center">
        <button class="btn waves-effect waves-light blue darken-2" type="submit" name="action">@lang('auth/signup.submit')
            <i class="material-icons right">send</i>
        </button></div>
    <div class="col s6 center">
        <button class="btn waves-effect waves-light blue darken-2" type="reset" name="action">@lang('auth/signup.reset')
            <i class="material-icons right">undo</i>
        </button>
    </div>
</div>

{{ Form::close() }}