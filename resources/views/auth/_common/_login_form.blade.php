{{ Form::open(['route' => 'session.store', 'method' => 'post', 'class' => 'col s12', 'autocomplete' => 'on']) }}

<div class="row">
    <div class="input-field col s12">
        {{ Form::email('email', null, ['id' => 'email', 'class' => $errors->has('email') ? 'validate invalid' : 'validate']) }}
        <label for="email" data-error="{{ $errors->first('email') }}">@lang('auth/signin.email')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::password('password', ['id' => 'password', 'class' => $errors->has('password') ? 'validate invalid' : 'validate']) }}
        <label for="password" data-error="{{ $errors->first('password') }}">@lang('auth/signin.password')</label>
    </div>
</div>

<div class="row">
    <div class="center">
        {{ Form::checkbox('remember', null, null, ['id' => 'filled-in-box', 'class' => 'filled-in', 'style' => 'left:0;']) }}
        <label for="filled-in-box">@lang('auth/signin.remember_me')</label>
    </div>
</div>

<div class="row">
    <div class="col s4">
        <button class="btn waves-effect waves-light light-blue darken-4" type="submit" name="action">@lang('auth/signin.sing_in')
            <i class="material-icons right">send</i>
        </button></div>
    <div class="col s4">
        <button class="btn waves-effect waves-light light-blue darken-4" type="reset" name="action">@lang('auth/signin.reset')
            <i class="material-icons right">undo</i>
        </button>
    </div>
    <div class="col s4">
        <a href="{{ route('reset.index') }}">@lang('auth/signin.forgot_password')</a>
    </div>
</div>

{{ Form::close() }}