{{ Form::open() }}

    <div class="row">
        <div class="input-field col s12">
            {{ Form::password('password', ['class' => 'validate', 'id' => 'password']) }}
            <label for="password" data-error="{{ $errors->first('password') }}">@lang('auth/reset_complete.new_password')</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::password('password_confirmation', ['class' => 'validate', 'id' => 'password-confirmation']) }}
            <label for="password-confirmation" data-error="{{ $errors->first('password_confirmation') }}">@lang('auth/reset_complete.confirm_new_password')</label>
        </div>
    </div>

    <div class="row">
        <div class="col s12">
            {{ Form::submit(__('auth/reset_complete.reset'), ['class' => 'btn waves-effect waves-light']) }}
        </div>
    </div>

{{ Form::close() }}