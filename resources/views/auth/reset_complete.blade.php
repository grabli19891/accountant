@extends('layouts.main')

@section('title', 'Reset Password')

@section('content')

    <div class="container">

        <div class="center">
            <h4>@lang('auth/reset_complete.reset_password')</h4>
        </div>

        @include('auth._common._reset_form')
    </div>

@stop