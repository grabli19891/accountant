@extends('layouts.main')

@section('title', 'Login')

@section('content')
<div class="container">

    <div class="row">
        <div class="center"><h4>@lang('auth/signin.login')</h4></div>
        <a href="{{ route('registration.create') }}">@lang('index.sign_up')</a>
         /
        <a href="{{ route('default') }}">בית</a>
        <div class="divider"></div>

    </div>
	<div class="row">
        <div class="col m2 l3">&nbsp;</div>
		<div class="col s12 l6 card-panel">
            <br>
            <br>

            @include('auth._common._login_form')
		</div>
        <div class="col m2 l3">&nbsp;</div>
    </div>
</div>
@endsection