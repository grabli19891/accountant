@extends('layouts.main')

@section('title', 'Registration')

@section('content')
<div class="container">

    <div class="row">
        <div class="center"><h4>@lang('auth/signup.register')</h4></div>
        <a href="{{ route('default') }}">בית</a>
        <div class="divider"></div>

    </div>
    <div class="row">
        <div class="col m2 l3">&nbsp;</div>
        <div class="col s12 m8 l6 card-panel">
            <br>
            <br>

            @include('auth._common._register_form')
        </div>
        <div class="col m2 l3">&nbsp;</div>
    </div>
</div>
@stop
