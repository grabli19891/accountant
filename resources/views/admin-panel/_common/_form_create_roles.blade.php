{{ Form::open(['route' => 'roles.store']) }}
<div class="row">
    <div class="input-field col s12">
        {{ Form::text('slug', null, ['id' => 'slug', 'class' => $errors->has('slug') ? 'validate invalid' : 'validate ']) }}
        <label for="slug" data-error="{{ $errors->first('slug') }}">@lang('admin/roles.slug')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        {{ Form::text('name', null, ['id' => 'name', 'class' => $errors->has('name') ? 'validate invalid' : 'validate ']) }}
        <label for="name" data-error="{{ $errors->first('name') }}">@lang('admin/roles.name')</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s4">
        {{ Form::textarea('permissions', null, ['id' => 'permissions', 'class' => $errors->has('permissions') ? 'materialize-textarea validate invalid' : 'materialize-textarea validate ']) }}
        <label for="permissions" data-error="{{ $errors->first('permissions') }}">@lang('admin/roles.permissions')</label>
    </div>
    <div class="col s4">
        <span>Example:</span>
        <p>
            {<br>"admin":false,<br>"superadmin":false,<br>"moderator":true<br>}
        </p>
    </div>
</div>

<div class="row">
    <div class="col s6 center">
        <button class="btn waves-effect waves-light blue darken-2" type="submit" name="action">@lang('admin/roles.submit')
            <i class="material-icons right">send</i>
        </button></div>
    <div class="col s6 center">
        <button class="btn waves-effect waves-light blue darken-2" type="reset" name="action">@lang('admin/roles.reset')
            <i class="material-icons right">undo</i>
        </button>
    </div>
</div>
{{ Form::close() }}