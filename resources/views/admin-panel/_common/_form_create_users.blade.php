{{ Form::open(['route' => 'users.store', 'method' => 'POST']) }}
    <div class="row">
        <div class="input-field col s12">
            {{ Form::email('email', null, ['id' => 'email', 'class' => $errors->has('email') ? 'validate invalid' : 'validate ']) }}
            <label for="email" data-error="{{ $errors->first('email') }}">@lang('admin/users.email')</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('first_name', null, ['id' => 'first_name', 'class' => $errors->has('first_name') ? 'validate invalid' : 'validate ']) }}
            <label for="first_name" data-error="{{ $errors->first('first_name') }}">@lang('admin/users.fname')</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('last_name', null, ['id' => 'last_name', 'class' => $errors->has('last_name') ? 'validate invalid' : 'validate ']) }}
            <label for="last_name" data-error="{{ $errors->first('last_name') }}">@lang('admin/users.lname')</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('phone', null, ['id' => 'phone', 'class' => $errors->has('phone') ? 'validate invalid phone' : 'validate ']) }}
            <label for="phone" data-error="{{ $errors->first('phone') }}">@lang('admin/users.phone')</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::password('password', ['id' => 'password', 'class' => $errors->has('password') ? 'validate invalid' : '', 'placeholder' => 'Enter the user password (only if you want to modify it).']) }}
            <label for="password" data-error="{{ $errors->first('password') }}">@lang('admin/users.password')</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::password('password_confirmation', ['id' => 'password_confirmation', 'class' => $errors->has('password_confirmation') ? 'validate invalid' : '']) }}
            <label for="password_confirmation" data-error="{{ $errors->first('password_confirmation') }}">@lang('admin/users.confirm_password')</label>
        </div>
    </div>

    @if(Sentinel::hasAccess('superadmin'))

        <div class="row">
            <div class="input-field col s12">
                {{ Form::select('roles[]', \Sentinel::getRoleRepository()->get()->pluck('name', 'id'), null,
                ['id' => 'roles', 'multiple' => 'multiple']) }}
                <label class="{{ $errors->has('roles') ? 'red-text' : '' }}">{{ ($errors->first('roles')) ? $errors->first('roles') : __('admin/users.user_roles') }}</label>
            </div>
        </div>

    @endif

    <div class="row">
        <div class="col s6 center">
            <button class="btn waves-effect waves-light blue darken-2" type="submit" name="action">@lang('admin/users.submit')
                <i class="material-icons right">send</i>
            </button></div>
        <div class="col s6 center">
            <button class="btn waves-effect waves-light blue darken-2" type="reset" name="action">@lang('admin/users.reset')
                <i class="material-icons right">undo</i>
            </button>
        </div>
    </div>
{{ Form::close() }}