@extends('layouts.admin-panel')

@section('title', 'Admin panel')

    @section('content')
        <div class="container">
            <div class="row">
                <nav>
                    <div class="nav-wrapper">
                        <ul id="nav-mobile" class="hide-on-med-and-down">
                            <li class="{{ isActiveRoute('admin.index') }}"><a href="{{ route('admin.index') }}">@lang('admin/index.info')</a></li>
                            <li class="{{ isActiveRoute('users.index') }}"><a href="{{ route('users.index') }}">@lang('admin/index.users')</a></li>
                            <li class="{{ isActiveRoute('roles.index') }}"><a href="{{ route('roles.index') }}">@lang('admin/index.roles')</a></li>
                            <li class="right"><a href="{{ route('default') }}">@lang('admin/index.main_page')</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="row">

                @yield('content.admin-panel')

            </div>
        </div>
    @endsection