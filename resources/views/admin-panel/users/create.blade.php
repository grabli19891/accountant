@extends('admin-panel.index')

@section('title, Create user')

    @section('content.admin-panel')

        @include('admin-panel._common._form_create_users')

    @endsection