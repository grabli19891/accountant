@extends('admin-panel.index')

@section('title', 'Users')

@section('content.admin-panel')
    <a href="{{ route('users.create') }}" class="waves-effect waves-light btn"><i class="material-icons left">add</i>@lang('admin/users.add_new_user')</a>
    <table class="responsive-table">
        <thead>
        <tr>
            <th>id</th>
            <th>@lang('admin/users.email')</th>
            <th>@lang('admin/users.fname')</th>
            <th>@lang('admin/users.lname')</th>
            <th>@lang('admin/users.phone')</th>
            <th>@lang('admin/users.last_login')</th>
            <th>@lang('admin/users.actions')</th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->phone }}</td>
            <td>{{ $user->last_login }}</td>
            <td>
                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-floating waves-effect waves-light yellow"><i class="material-icons">mode_edit</i></a>

                {{ Form::open(['route' => ['users.destroy', $user->id], 'style' => 'display: inline;']) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<i class="material-icons">delete_forever</i>', ['type' => 'submit', 'class' => 'btn btn-floating waves-effect waves-light red']) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection