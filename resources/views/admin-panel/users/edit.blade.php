@extends('admin-panel.index')

@section('title, Edit user')

    @section('content.admin-panel')

        @include('admin-panel._common._form_edit_users')

    @endsection