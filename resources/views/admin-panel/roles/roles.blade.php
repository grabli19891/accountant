@extends('admin-panel.index')

@section('title', 'Roles')

@section('content.admin-panel')
    <a href="{{ route('roles.create') }}" class="waves-effect waves-light btn"><i class="material-icons left">add</i>@lang('admin/roles.add_new_role')</a>
    <table class="responsive-table">
        <thead>
        <tr>
            <th>id</th>
            <th>@lang('admin/roles.slug')</th>
            <th>@lang('admin/roles.name')</th>
            <th>@lang('admin/roles.permissions')</th>
            <th>@lang('admin/roles.actions')</th>
        </tr>
        </thead>

        <tbody>
        @foreach($roles as $role)
            <tr>
                <td>{{ $role->id }}</td>
                <td>{{ $role->slug }}</td>
                <td>{{ $role->name }}</td>
                <td>{{ json_encode($role->permissions) }}</td>
                <td>
                    <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-floating waves-effect waves-light yellow"><i class="material-icons">mode_edit</i></a>

                    {{ Form::open(['route' => ['roles.destroy', $role->id], 'style' => 'display: inline;']) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::button('<i class="material-icons">delete_forever</i>', ['type' => 'submit', 'class' => 'btn btn-floating waves-effect waves-light red']) }}
                    {{ Form::close() }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection