@extends('admin-panel.index')

@section('title, Create role')

@section('content.admin-panel')

    @include('admin-panel._common._form_create_roles')

@endsection