@extends('admin-panel.index')

@section('title, Edit role')

@section('content.admin-panel')

    @include('admin-panel._common._form_edit_roles')

@endsection