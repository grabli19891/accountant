<?php
return [

    'hello' => 'Hello',
    'logout' => 'Logout',
    'dashboard' => 'Dashboard',
    'sign_in' => 'Sign In',
    'sign_up' => 'Sign Up',
    'admin_panel' => 'Admin panel',

];