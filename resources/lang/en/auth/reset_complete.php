<?php
return [
    'reset_password' => 'Reset Password',
    'new_password' => 'New Password',
    'confirm_new_password' => 'Confirm New Password',
    'reset' => 'Reset',
];