<?php

return [
    'login' => 'Login',
    'email' => 'Email',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'sing_in' => 'Sign in',
    'reset' => 'Reset',
    'forgot_password' => 'Forgot password?',

];