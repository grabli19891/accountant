<?php

return [
    'register' => 'Register',
    'email' => 'Email',
    'fname' => 'First name',
    'lname' => 'Last name',
    'phone' => 'Phone',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'submit' => 'Submit',
    'reset' => 'Reset',
];