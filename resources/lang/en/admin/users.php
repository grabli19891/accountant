<?php
return [
    'add_new_user' => 'add new user',
    'email' => 'Email',
    'fname' => 'First name',
    'lname' => 'Last name',
    'phone' => 'Phone',
    'last_login' => 'Last login',
    'actions' => 'Actions',

    // Form
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'user_roles' => 'User roles',
    'submit' => 'Submit',
    'reset' => 'Reset',
];