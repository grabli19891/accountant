<?php
return [
    'dashboard' => 'Dashboard',
    'select_client' => 'Select client',
    'all' => 'All...',
    'client' => 'Client',
    'name' => 'Name',
    'settings' => 'Settings',
    'status' => 'Status',
    'actions' => 'Actions',
    'complete' => 'Complete',
];