<?php
return [
    'clients' => 'Clients',
    'create' => 'Create',
    'client_number' => 'Client number',
    'name' => 'Name',
    'passport' => 'Passport',
    'tiknikuim' => 'Tik nikuim number',
    'phone' => 'Phone',
    'email' => 'Email',
    'actions' => 'Actions',

];