<?php
return [
    'number' => 'Number',
    'name' => 'Name',
    'passport' => 'Passport',
    'tiknikuim' => 'Tik nikuim',
    'phone' => 'Phone',
    'email' => 'Email',
    'submit' => 'Submit',
    'reset' => 'Reset',
];