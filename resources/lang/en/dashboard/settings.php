<?php
return [
    'settings' => 'Settings',
    // Custom reports
    'custom_reports' => 'Custom reports',
    'name' => 'Name',
    'clients' => 'Clients',
    'actions' => 'Actions',
    // Add custom report
    'add_custom_report' => 'Add custom report',
    'type_name' => 'Type name for new report',
    // Profile
    'profile' => 'Profile',
    'email' => 'Email',
    'fname' => 'First name',
    'lname' => 'Last name',
    'phone' => 'Phone',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'submit' => 'Submit',
    'reset' => 'Reset'
];

