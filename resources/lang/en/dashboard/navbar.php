<?php
return [
    'dashboard' => 'Dashboard',
    'clients' => 'Clients',
    'report' => 'Report',
    'settings' => 'Settings',
    'logout' => 'Logout',
];