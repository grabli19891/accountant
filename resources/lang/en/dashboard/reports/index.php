<?php
return [
    'reports' => 'Reports',
    'select_report' => 'Select report',
    'select_type_report' => 'Select type report',
    'select_other_type' => 'Reports not found, select other type',
    'complete' => 'Complete',
    'settings' => 'Settings',
    'not_settings' => 'Not settings',
    'client_name' => 'Client name',
    'todo_items' => 'Todo items',
    'comments' => 'Comments',
    'carry_out' => 'Carry out',
];