<?php

return [
    'login' => 'כניסה למערכת',
    'email' => 'דוא"ל',
    'password' => 'סיסמא',
    'remember_me' => 'זכור אותי',
    'sing_in' => 'כנס',
    'reset' => 'לאפס',
    'forgot_password' => 'שכחת סיסמא?',

];