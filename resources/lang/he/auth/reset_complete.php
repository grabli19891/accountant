<?php
return [
    'reset_password' => 'איפוס סיסמא',
    'new_password' => 'סיסמא חדשה',
    'confirm_new_password' => 'אימות סיסמא',
    'reset' => 'שלח',
];