<?php

return [
    'register' => 'הרשמה למערכת',
    'email' => 'דוא"ל',
    'fname' => 'שם',
    'lname' => 'שם משפחה',
    'phone' => 'טלפון',
    'password' => 'סיסמא',
    'confirm_password' => 'אימות סיסמא',
    'submit' => 'שלח',
    'reset' => 'אפס',
];