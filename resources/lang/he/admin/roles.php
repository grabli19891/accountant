<?php
return [
    'add_new_role' => 'Add new role',
    'slug' => 'Slug',
    'name' => 'Name',
    'permissions' => 'Permissions',
    'actions' => 'Actions',

    // Form
    'submit' => 'Submit',
    'reset' => 'Reset',

];