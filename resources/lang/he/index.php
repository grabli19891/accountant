<?php
return [

    'hello' => 'שלום',
    'logout' => 'לצאת',
    'dashboard' => 'פאנל נהיול',
    'sign_in' => 'כניסה',
    'sign_up' => 'הרשמה',
    'admin_panel' => 'אדמין פאנל',

];