<?php
return [
    'settings' => 'הגדרות',
    // Custom reports
    'custom_reports' => 'דוחות אקראיים',
    'name' => 'דוח',
    'clients' => 'לקוחות',
    'actions' => 'פעילות',
    // Add custom report
    'add_custom_report' => 'הוסף דוח אקראי',
    'type_name' => 'בחר שם לדוח אקראי',
    // Profile
    'profile' => 'פרטים אישיים',
    'email' => 'דוא"ל',
    'fname' => 'שם',
    'lname' => 'שם משפחה',
    'phone' => 'טלפון',
    'password' => 'סיסמא',
    'confirm_password' => 'אימות סיסמא',
    'submit' => 'שלח',
    'reset' => 'אפס'
];

