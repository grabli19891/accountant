<?php
return [
    'dashboard' => 'פאנל ניהול',
    'select_client' => 'בחר לקוח',
    'all' => 'כולם...',
    'client' => 'לקוח',
    'name' => 'דוח',
    'settings' => 'הגדרות',
    'status' => 'סטטוס',
    'actions' => 'פעילות',
    'complete' => 'סיום',
    'report_completed' => 'בוצע בהצלחה'
];