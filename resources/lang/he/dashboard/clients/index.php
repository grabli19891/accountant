<?php
return [
    'clients' => 'לקוחות',
    'create' => 'הוספת לקוח',
    'client_number' => 'מס לקוח',
    'name' => 'שם',
    'passport' => 'ת.ז. / ח.פ.',
    'tiknikuim' => 'מספר תיק ניקויים',
    'phone' => 'טלפון',
    'email' => 'דוא"ל',
    'actions' => 'פעילות',

];