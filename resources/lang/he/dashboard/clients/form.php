<?php
return [
    'number' => 'מספר לקוח',
    'name' => 'שם',
    'passport' => 'ת.ז. / ח.פ.',
    'tiknikuim' => 'מספר תיק ניקויים',
    'phone' => 'טלפון',
    'email' => 'דוא"ל',
    'submit' => 'שלח',
    'reset' => 'אפס',
];