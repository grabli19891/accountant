<?php
return [
    'dashboard' => 'פאנל ניהול',
    'clients' => 'לקוחות',
    'report' => 'דוחות',
    'settings' => 'הגדרות',
    'logout' => 'יציאה',
];