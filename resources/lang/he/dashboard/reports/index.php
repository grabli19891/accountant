<?php
return [
    'reports' => 'דוחות',
    'select_report' => 'בחר דוח',
    'select_type_report' => 'בחר דוח',
    'select_other_type' => 'דוח ריק',
    'complete' => 'בצע',
    'settings' => 'הגדרות',
    'not_settings' => '-',
    'client_name' => 'שם לקוח',
    'todo_items' => 'משימות',
    'comments' => 'הערות',
    'carry_out' => 'סיום',
];