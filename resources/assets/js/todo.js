$(document).ready(function () {
    $('.delete-todo').on('click', todoDelete);

    let todoStore = function (data) {
        return axios({
            method: 'POST',
            url: '/dashboard/todos',
            data: data
        });
    }

    $('.report-list').on('click', '.add-todo-link', function (event) {
        var input = $(this).closest('.todos').find('.input-field');
        input.show();
        $(input).find('input').focus();
        $(input).find('input').on('keyup', function (e) {
            if(e.keyCode == 13) {
                $(this).closest('.todos').find('.append-todo').click();
            }
        });
        $(this).hide();
        $(this).closest('.todos').find('.append-todo').removeClass('hide');
    });

    $('.report-list').on('click', '.append-todo', function () {
        var el = this;
        let name = $(this).closest('.todos').find("input[type='text']").val();
        name = name.trim();
        if(!name) {
            console.log('need todo name')
            return;
        }
        let report_id = $(this).closest('.todos').find("input[name='report_id']").val();
        let client_id = $("input[name='client_id']").val();
        let data = {
            client_id: client_id,
            report_id: report_id,
            name: name
        }
        todoStore(data)
            .then(
                function (response) {
                    $(el).closest('.todos').find("input[type='text']").val('');
                    let newLi = document.createElement('li');
                    $(newLi).addClass('todo').addClass('collection-item');

                    let div = document.createElement('div');

                    let span = document.createElement('span');
                    $(span).addClass('title');

                    let b_title = document.createElement('b');
                    let b_status = document.createElement('b');

                    $(b_title).text('משימה: ');
                    $(b_status).text(' /סטטוס: ');

                    $(span).append(b_title);
                    $(span).append(response.data.name);
                    $(span).append(b_status);
                    $(span).append(response.data.status);

                    // $(div).text(response.data.name + ' / ' + response.data.status);
                    $(div).append(span);

                    let deleteButton = document.createElement('a');
                    $(deleteButton).addClass('secondary-content');
                    $(deleteButton).addClass('delete-todo');
                    $(deleteButton).data('todo_id', response.data.id);
                    $(deleteButton).attr('href', '#!');
                    $(deleteButton).on('click', todoDelete);

                    let icon = document.createElement('i');
                    $(icon).addClass('material-icons');
                    $(icon).text('delete');
                    $(deleteButton).append(icon);
                    $(div).append(deleteButton);

                    $(newLi).append(div);
                    $(el).closest('.todos').find('ul').append(newLi);
                    $(el).closest('.todos').find("input[type='text']").focus();
                }
            )
            .catch(function (response) {
                console.log('add todo error:', response);
            })


    });

    $('.todoCompleteButton').on('click', function () {
        var el = this;
        var todo_id = $(el).data('todo_id');
        todoComplete(todo_id)
            .then(
                function (response) {
                    var model = response.data;
                    $(el).find('.status').text(model.status);
                    $(el).closest('li').fadeOut('fast', () => {
                        $(this.remove());
                    });
                }
            )
    });

});

var todoDelete = function (e) {
    let element = e.currentTarget;
    let todo_id = $(element).data('todo_id');
    let report_id = $(element).closest('.todos').find("input[name='report_id']").val();
    axios({
        method: 'DELETE',
        url: '/dashboard/todos/' + todo_id
    })
        .then(
            function (response) {
                $(element).closest('li').slideUp('fast', function () {
                    $(this).remove();
                });
            }
        )
        .catch(function (response) {
            console.log('error delete todo', response);
        });
}

var todoComplete = function (todo_id) {
    return axios({
        method: 'GET',
        url: '/dashboard/todos/' + todo_id + '/complete'
    });
}