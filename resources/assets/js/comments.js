$(document).ready(function () {
    $('.delete-comment').on('click', commentDelete);



    let commentStore = function (data) {
        return axios({
            method: 'POST',
            url: '/dashboard/comments',
            data: data
        });
    }

    $('.report-list').on('click', '.add-comment-link', function (event) {
    // $('.add-comment-link').on('click', function (event) {
        var input = $(this).closest('.comments').find('.input-field');
        input.show();
        $(input).find('input').focus();
        $(input).find('input').on('keyup', function (e) {
            if(e.keyCode == 13) {
                $(this).closest('.comments').find('.append-comment').click();
            }
        });
        $(this).hide();
        $(this).closest('.comments').find('.append-comment').removeClass('hide');
    });

    $('.report-list').on('click', '.append-comment', function () {
        var el = this;
        let comment = $(this).closest('.comments').find("input[type='text']").val();
        comment = comment.trim();
        if(!comment) {
            return;
        }
        let report_id = $(this).closest('.comments').find("input[name='report_id']").val();
        let client_id = $("input[name='client_id']").val();
        let data = {
            client_id: client_id,
            report_id: report_id,
            comment: comment
        }
        commentStore(data)
            .then(
                function (response) {
                    $(el).closest('.comments').find("input[type='text']").val('');
                    let newLi = document.createElement('li');
                    $(newLi).addClass('comment').addClass('collection-item');

                    let div = document.createElement('div');
                    $(div).text(response.data.comment);

                    let deleteButton = document.createElement('a');
                    $(deleteButton).addClass('secondary-content');
                    $(deleteButton).addClass('delete-comment');
                    $(deleteButton).data('comment_id', response.data.id);
                    $(deleteButton).attr('href', '#!');
                    $(deleteButton).on('click', commentDelete);

                    let icon = document.createElement('i');
                    $(icon).addClass('material-icons');
                    $(icon).text('delete');
                    $(deleteButton).append(icon);
                    $(div).append(deleteButton);

                    $(newLi).append(div);
                    $(el).closest('.comments').find('ul').append(newLi);
                    $(el).closest('.comments').find("input[type='text']").focus();
                }
            )
            .catch(function (response) {
                console.log('add comment error:', response);
            })


    });

});

var commentDelete = function (e) {
    let element = e.currentTarget;
    let comment_id = $(element).data('comment_id');
    let report_id = $(element).closest('.comments').find("input[name='report_id']").val();
    axios({
        method: 'DELETE',
        url: '/dashboard/comments/' + comment_id
    })
        .then(
            function (response) {
                $(element).closest('li').slideUp('fast', function () {
                    $(this).remove();
                });
            }
        )
        .catch(function (response) {
            console.log('error delete comment', response);
        });
}