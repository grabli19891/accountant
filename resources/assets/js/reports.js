$(document).ready(function () {
    var client_id = $("input[name='client_id']").val();

    let ajaxReportAttach = function (data) {
        if (!data.settings) {
            data.settings = {some: ''}
        }
        console.log(data);
        return axios({
            method: 'post',
            url: `/dashboard/clients/${client_id}/attachReport/`,
            data: data
        });
    }

    let reportComplete = function (data) {
        if (!client_id || !data) {
            return;
        }
        console.log(client_id, data);
        return axios({
            method: 'post',
            url: `/dashboard/clients/${client_id}/reportComplete/`,
            data: data
        });
    }

    $('.complete-button').on('click', function () {
        var el = this;
        var report_id = $(el).data('report_id');
        var type = $(el).data('type');
        client_id = $(el).data('client_id');
        reportComplete({'type': type, report_id: report_id})
            .then(
                function (response) {
                    var tr = $(el).closest('tr');
                    tr.fadeOut('fast');
                    $(document).trigger("remRow", [tr]);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    // Client filter
    $('#clients_filter').on('change', function () {
        var value = $(this).val();
        $(document).trigger("clientsFilter", [value]);
    });

    $('.complete-button-in-reports').on('click', function () {
        var el = this;
        var report_id = $(el).data('report_id');
        var type = $(el).data('type');
        client_id = $(el).data('client_id');
        reportComplete({'type': type, report_id: report_id})
            .then(
                function (response) {
                    $(el).addClass('disabled');
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    // Get years for annual report
    let getYears = function (finishYear) {
        var currentYear = new Date().getFullYear(), years = [];
        finishYear = finishYear || currentYear + 20;

        while (currentYear <= finishYear) {
            years.push(currentYear++);
        }

        return years;
    }

    let years = getYears(2025);

    for (let el in years) {
        var opt = document.createElement("option");
        opt.value = years[el];
        opt.innerHTML = years[el]; // whatever property it has

        // then append it to the select element
        $('#annualReportYear').append(opt);
    }

    $('#annualReportSwitch').click(function () {
        $('#annualReportYear').attr('disabled', !this.checked);
        $('select').material_select();
    });

    $('#annualReportYear').on('change', function (e) {
        {
            var el = this;
            let year = $('#annualReportYear').val();
            if (!year) {
                console.log('need year');
                return;
            }
            ajaxReportAttach({
                'settings': {'year': year},
                'type': 'annual-report'
            })
                .then(
                    function (response) {
                        var model = response.data;
                        $('#year-button').text(year);
                        $('#year-button').attr('disabled', true);
                        $('#annualReportSwitch').attr('disabled', true);
                        $('#annualReportComplete').attr('disabled', false);
                        var report_id = model.id;
                        var comments = $(el).closest('.collapsible-body').find('.comments');
                        comments.removeClass('hide');
                        comments.find("input[name='report_id']").val(report_id);
                        comments.find('.comments_list').empty();
                    }
                )
                .catch(
                    function (response) {
                        console.log('err', response);
                    }
                );
        }
    });
    $('#annualReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'annual-report', 'report_id': report_id})
            .then(
                function (response) {
                    $('#annualReportYear').attr('disabled', true);
                    $('select').material_select();
                    $('#annualReportSwitch').attr('disabled', false);
                    $('#annualReportComplete').attr('disabled', true);
                    $('#annualReportSwitch').prop('checked', false);
                    var comments = $('#annualReportComplete').closest('.collapsible-body').find('.comments');
                    comments.addClass('hide');
                    comments.find("input[name='report_id']").val(0);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    //Declaration of capital
    $('#declarationOfCapitalReportSwitch').click(function () {
        $('#declarationOfCapitalReportDate').attr('disabled', !this.checked);
    });

    $('#declarationOfCapitalReportDate').on('change', function (e) {
        {
            var el = this;
            let date = $('#declarationOfCapitalReportDate').val();
            if (!date) {
                console.log('need date');
                return;
            }
            ajaxReportAttach({
                'settings': {'date': date},
                'type': 'declaration-of-capital'
            })
                .then(
                    function (response) {
                        var model = response.data;
                        $('#declarationOfCapitalReportDate').attr('disabled', true);
                        $('#declarationOfCapitalReportSwitch').attr('disabled', true);
                        $('#declarationOfCapitalReportComplete').attr('disabled', false);
                        var report_id = model.id;
                        var comments = $(el).closest('.collapsible-body').find('.comments');
                        comments.removeClass('hide');
                        comments.find("input[name='report_id']").val(report_id);
                        comments.find('.comments_list').empty();
                    }
                )
                .catch(
                    function (response) {
                        console.log('err', response);
                    }
                );
        }
    });

    $('#declarationOfCapitalReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'declaration-of-capital', 'report_id': report_id})
            .then(
                function (response) {
                    $('#declarationOfCapitalReportSwitch').attr('disabled', false);
                    $('#declarationOfCapitalReportComplete').attr('disabled', true);
                    $('#declarationOfCapitalReportSwitch').prop('checked', false);
                    var comments = $(el).closest('.collapsible-body').find('.comments');
                    comments.addClass('hide');
                    comments.find("input[name='report_id']").val(0);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    //Ishur nikui mas bamakor

    var dateAlert = function (timestamp) {
        return timestamp - new Date().getTime() < 1000 * 60 * 60 * 24 * 7;
    }

    $('#IshurNikuiMasBamakorReportSwitch').click(function () {
        $('#ishur-nikui-mas-bamakor-pecents').attr('disabled', !this.checked);
        $('#ishur-nikui-mas-bamakor-pecents').focus();

    });

    $('#ishur-nikui-mas-bamakor-pecents').on('change', function () {
        var el = this;
        var value = $(el).val();
        if (value) {
            $('#IshurNikuiMasBamakorReportDate').attr('disabled', false);
        } else {
            $('#IshurNikuiMasBamakorReportDate').attr('disabled', true);
        }
    });

    $('#IshurNikuiMasBamakorReportDate').on('change', function (e) {
        {
            var el = this;
            let date = $('#IshurNikuiMasBamakorReportDate').val();
            let percents = $('#ishur-nikui-mas-bamakor-pecents').val();
            if (!date || !percents) {
                console.log('need date and percents');
                return;
            }
            ajaxReportAttach({
                'settings': {'date': date, 'percents': percents},
                'type': 'ishur-nikui-mas-bamakor'
            })
                .then(
                    function (response) {
                        var comments = $(el).closest('.collapsible-body').find('.comments');
                        var model = response.data;
                        $('#ishur-nikui-mas-bamakor-pecents').val(model.settings.percents);
                        $('#ishur-nikui-mas-bamakor-pecents').attr('disabled', true);
                        $('#IshurNikuiMasBamakorReportDate').attr('disabled', true);
                        $('#IshurNikuiMasBamakorReportSwitch').attr('disabled', true);
                        $('#IshurNikuiMasBamakorReportComplete').attr('disabled', false);
                        var report_id = model.id;
                        comments.removeClass('hide');
                        comments.find("input[name='report_id']").val(report_id);
                        //date alert
                        if (dateAlert(new Date(Date.parse(date)).getTime())) {
                            $(el).closest('.collapsible-body').find('.date-alert').removeClass('hide');
                        }
                    }
                )
                .catch(
                    function (response) {
                        console.log('err', response);
                    }
                );
        }
    });

    $('#IshurNikuiMasBamakorReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'ishur-nikui-mas-bamakor', 'report_id': report_id})
            .then(
                function (response) {
                    var comments = $(el).closest('.collapsible-body').find('.comments');
                    $('#ishur-nikui-mas-bamakor-pecents').val(null);
                    $('#IshurNikuiMasBamakorReportDate').val(null);
                    $('#IshurNikuiMasBamakorReportSwitch').attr('disabled', false);
                    $('#IshurNikuiMasBamakorReportComplete').attr('disabled', true);
                    $('#IshurNikuiMasBamakorReportSwitch').prop('checked', false);
                    comments.addClass('hide');
                    comments.find("input[name='report_id']").val(0);
                    comments.find('.comments_list').empty();
                    $(el).closest('.collapsible-body').find('.date-alert').addClass('hide');
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });


    //Mam
    $('#MamReportSwitch').click(function () {
        var el = this;
        ajaxReportAttach({
            'type': 'mam'
        })
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    var model = response.data;
                    $('#MamReportSwitch').attr('disabled', true);
                    $('#MamReportComplete').attr('disabled', false);
                    var report_id = model.id;
                    todos.removeClass('hide');
                    todos.find("input[name='report_id']").val(report_id);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    $('#MamReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'mam', 'report_id': report_id})
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    $('#MamReportSwitch').attr('disabled', false);
                    $('#MamReportComplete').attr('disabled', true);
                    $('#MamReportSwitch').prop('checked', false);
                    todos.addClass('hide');
                    todos.find("input[name='report_id']").val(0);
                    todos.find('.todos_list').empty();
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });


    //Mas ahnasa
    $('#MasAhnasaReportSwitch').click(function () {
        var el = this;
        ajaxReportAttach({
            'type': 'mas-ahnasa'
        })
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    var model = response.data;
                    $('#MasAhnasaReportSwitch').attr('disabled', true);
                    $('#MasAhnasaReportComplete').attr('disabled', false);
                    var report_id = model.id;
                    todos.removeClass('hide');
                    todos.find("input[name='report_id']").val(report_id);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    $('#MasAhnasaReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'mas-ahnasa', 'report_id': report_id})
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    $('#MasAhnasaReportSwitch').attr('disabled', false);
                    $('#MasAhnasaReportComplete').attr('disabled', true);
                    $('#MasAhnasaReportSwitch').prop('checked', false);
                    todos.addClass('hide');
                    todos.find("input[name='report_id']").val(0);
                    todos.find('.todos_list').empty();
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    //Bituah leumi
    $('#BituahLeumiReportSwitch').click(function () {
        var el = this;
        ajaxReportAttach({
            'type': 'bituach-leumi'
        })
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    var model = response.data;
                    $('#BituahLeumiReportSwitch').attr('disabled', true);
                    $('#BituahLeumiReportComplete').attr('disabled', false);
                    var report_id = model.id;
                    todos.removeClass('hide');
                    todos.find("input[name='report_id']").val(report_id);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    $('#BituahLeumiReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'bituach-leumi', 'report_id': report_id})
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    $('#BituahLeumiReportSwitch').attr('disabled', false);
                    $('#BituahLeumiReportComplete').attr('disabled', true);
                    $('#BituahLeumiReportSwitch').prop('checked', false);
                    todos.addClass('hide');
                    todos.find("input[name='report_id']").val(0);
                    todos.find('.todos_list').empty();
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });


    //Itra
    $('#ItraReportSwitch').click(function () {
        $('#itra-sum').attr('disabled', !this.checked);
        $('#itra-sum').focus();
    });

    $('#itra-sum').on('keyup mouseup', function (e) {
        if (this.value) {
            $('#itra-activate').removeClass('disabled');
        } else {
            $('#itra-activate').addClass('disabled');
        }
        if (e.keyCode == 13 && this.value) {
            $('#itra-activate').click();
        }
    });

    $('#itra-activate').on('click', function (e) {
        {
            var el = this;
            let sum = $('#itra-sum').val();
            if (!sum.trim()) {
                console.log('need sum');
                return;
            }
            ajaxReportAttach({
                'settings': {'sum': sum},
                'type': 'itra'
            })
                .then(
                    function (response) {
                        var comments = $(el).closest('.collapsible-body').find('.comments');
                        var model = response.data;
                        $('#itra-activate').addClass('disabled');
                        $('#itra-sum').attr('disabled', true);
                        $('#ItraReportSwitch').attr('disabled', true);
                        $('#ItraReportComplete').attr('disabled', false);
                        var report_id = model.id;
                        comments.removeClass('hide');
                        comments.find("input[name='report_id']").val(report_id);
                    }
                )
                .catch(
                    function (response) {
                        console.log('err', response);
                    }
                );
        }
    });

    $('#ItraReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'itra', 'report_id': report_id})
            .then(
                function (response) {
                    var comments = $(el).closest('.collapsible-body').find('.comments');
                    $('#itra-sum').val(null);
                    $('#ItraReportSwitch').attr('disabled', false);
                    $('#ItraReportComplete').attr('disabled', true);
                    $('#ItraReportSwitch').prop('checked', false);
                    comments.addClass('hide');
                    comments.find("input[name='report_id']").val(0);
                    comments.find('.comments_list').empty();
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    //Tipulim shonim

    $('#TipulimShonimReportSwitch').click(function () {
        var el = this;
        ajaxReportAttach({
            'type': 'tipulim-shonim'
        })
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    var model = response.data;
                    $('#TipulimShonimReportSwitch').attr('disabled', true);
                    $('#TipulimShonimReportComplete').attr('disabled', false);
                    var report_id = model.id;
                    todos.removeClass('hide');
                    todos.find("input[name='report_id']").val(report_id);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    $('#TipulimShonimReportComplete').on('click', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'tipulim-shonim', 'report_id': report_id})
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    $('#TipulimShonimReportSwitch').attr('disabled', false);
                    $('#TipulimShonimReportComplete').attr('disabled', true);
                    $('#TipulimShonimReportSwitch').prop('checked', false);
                    todos.addClass('hide');
                    todos.find("input[name='report_id']").val(0);
                    todos.find('.todos_list').empty();
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    $('.report-list').on('click', '.custom-report-switch', function () {
        var el = this;
        var li = $(el).closest('.report-node');
        var cr_id = $(li).find("input[name='cr_id']").val();
        var name = li.find('.caption-report').text();
        if (!name.trim()) {
            return;
        }
        ajaxReportAttach({
            'settings': {'name': name, 'cr_id': cr_id},
            'type': 'custom'
        })
            .then(
                function (response) {
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    var model = response.data;
                    $(el).attr('disabled', true);
                    li.find('.custom-report-complete').attr('disabled', false);
                    var report_id = model.id;
                    todos.removeClass('hide');
                    todos.find("input[name='report_id']").val(report_id);
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });

    $('.report-list').on('click', '.custom-report-complete', function (e) {
        var el = this;
        var report_node = $(el).closest('.report-node');
        var report_id = $(report_node).find("input[name='report_id']").val();
        reportComplete({'type': 'custom', report_id: report_id})
            .then(
                function (response) {
                    var toggle = report_node.find('.custom-report-switch');
                    var todos = $(el).closest('.collapsible-body').find('.todos');
                    $(toggle).attr('disabled', false);
                    $(el).attr('disabled', true);
                    $(toggle).prop('checked', false);
                    todos.addClass('hide');
                    todos.find("input[name='report_id']").val(0);
                    todos.find('.todos_list').empty();
                }
            )
            .catch(
                function (response) {
                    console.log('err', response);
                }
            );
    });
});
