$(document).ready(function () {

    $(document).ready(function(){
        $('ul.tabs').tabs();
    });

    var dirChevrons = function () {
        var dir = $('html').attr('dir');
        var chevrons = { previous: null, next: null};
        if(dir == 'rtl') {
            chevrons.previous = "<a href='#!'><i class='material-icons'>chevron_right</i></a>";
            chevrons.next = "<a href='#!'><i class='material-icons'>chevron_left</i></a>";
        } else {
            chevrons.previous = "<a href='#!'><i class='material-icons'>chevron_left</i></a>";
            chevrons.next = "<a href='#!'><i class='material-icons'>chevron_right</i></a>";
        }
        return chevrons;
    }

    // $('select').material_select();
    $(".button-collapse").sideNav({
        menuWidth: 250,
        edge:"right"
    });
    // $('.modal').modal({
    //     dismissible: true, // Modal can be dismissed by clicking outside of the modal
    //     opacity: 0, // Opacity of modal background
    // });

    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year
        onSet: function (thingSet) {
            this.close();
        },
        monthsFull: [ 'ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר' ],
        monthsShort: [ 'ינו', 'פבר', 'מרץ', 'אפר', 'מאי', 'יונ', 'יול', 'אוג', 'ספט', 'אוק', 'נוב', 'דצמ' ],
        weekdaysFull: [ 'יום ראשון', 'יום שני', 'יום שלישי', 'יום רביעי', 'יום חמישי', 'יום ששי', 'יום שבת' ],
        weekdaysShort: [ 'א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ש' ],
        today: 'היום',
        clear: 'למחוק',
        format: 'yyyy mmmmב d dddd',
        formatSubmit: 'yyyy/mm/dd',
        close: 'קרוב'
    });

    var table = $('#reports').DataTable({
        "order": [[0, "asc"]],
        "dom": '<"top"fp<"clear">>rt<"bottom"il<"clear">>',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        language: {
            "processing":   "מעבד...",
            "lengthMenu":   "הצג _MENU_ פריטים",
            "zeroRecords":  "לא נמצאו רשומות מתאימות",
            "emptyTable":   "לא נמצאו רשומות מתאימות",
            "info": "_START_ עד _END_ מתוך _TOTAL_ רשומות" ,
            "infoEmpty":    "0 עד 0 מתוך 0 רשומות",
            "infoFiltered": "(מסונן מסך _MAX_  רשומות)",
            "infoPostFix":  "",
            "search":       "חפש:",
            "url":          "",
            "paginate": dirChevrons()
        }
    });

    $(document).on('remRow', function (event, tr) {
        window.setTimeout(function () {
            table.row(tr).remove().draw();
        }, 200);
        Materialize.toast(report_completed, 4000, 'green');
    });
    $(document).on('clientsFilter', function (event, value) {
        if (value == 'all') {
            $(document).trigger("resetFilter");
            return;
        }
        table.search(value).draw();
    });
    $(document).on('resetFilter', function () {
        table.search('').draw();
    });

    var client_table = $('#clients').DataTable({
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.15/i18n/Hebrew.json"
        },
        "order": [[1, "asc"]],
        "dom": '<"top"fp<"clear">>rt<"bottom"il<"clear">>',
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        language: {
            "processing":   "מעבד...",
            "lengthMenu":   "הצג _MENU_ פריטים",
            "zeroRecords":  "לא נמצאו רשומות מתאימות",
            "emptyTable":   "לא נמצאו רשומות מתאימות",
            "info": "_START_ עד _END_ מתוך _TOTAL_ רשומות" ,
            "infoEmpty":    "0 עד 0 מתוך 0 רשומות",
            "infoFiltered": "(מסונן מסך _MAX_  רשומות)",
            "infoPostFix":  "",
            "search":       "חפש:",
            "url":          "",
            "paginate": dirChevrons()
        }
    });

    client_table.$('tbody tr').on('click', 'td', function (e) {
        var el = this;
        console.log(el);
        window.location = $(el).parent().find('.client-show').attr('href');
    });

    $('select').material_select();

    $('.dataTables_wrapper .select-wrapper input').hide();
    $('.dataTables_wrapper .select-wrapper span').hide();
    $('.dataTables_wrapper .select-wrapper ul').hide();
    $('.dataTables_wrapper .select-wrapper select').addClass('browser-default');

});