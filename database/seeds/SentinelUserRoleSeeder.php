<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SentinelUserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_users')->delete();

        $superAdmin = Sentinel::findByCredentials(['login' => 'superadmin@admin.com']);
        $adminUser = Sentinel::findByCredentials(['login' => 'admin@admin.com']);
        $grblUser = Sentinel::findByCredentials(['login' => 'grabli1989@gmail.com']);

        $superAdminsRole = Sentinel::findRoleByName('SuperAdmins');
        $adminRole = Sentinel::findRoleByName('Admins');
        $userRole = Sentinel::findRoleByName('Users');

        // Assign the roles to the users
        $superAdminsRole->users()->attach($superAdmin);
        $adminRole->users()->attach($adminUser);
        $userRole->users()->attach($grblUser);

        $this->command->info('Users assigned to roles seeded!');
    }
}
