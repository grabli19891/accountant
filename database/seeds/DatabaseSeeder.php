<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(SentinelRoleSeeder::class);
        $this->call(SentinelUserSeeder::class);
        $this->call(SentinelUserRoleSeeder::class);


        factory(App\Models\Client::class, 25)->create();


        Model::reguard();
    }
}
