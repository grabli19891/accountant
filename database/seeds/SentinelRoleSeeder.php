<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SentinelRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Users',
            'slug' => 'users',
            'permissions' => [
                'user' => true,
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admins',
            'slug' => 'admins',
            'permissions' => [
                'admin' => true,
                'user' => true
            ]
        ]);

        Sentinel::getRoleRepository()->createModel()->create([
            'name' => 'SuperAdmins',
            'slug' => 'superadmins',
            'permissions' => [
                'superadmin' => true,
                'admin' => true,
                'user' => true
            ]
        ]);
      $this->command->info('Roles seeded!');
    }
}
