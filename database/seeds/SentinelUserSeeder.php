<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SentinelUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        Sentinel::registerAndActivate([
            'email'    => 'superadmin@admin.com',
            'password' => 'superadmin',
            'first_name' => 'Superadmin',
            'last_name' => '',
        ]);

        Sentinel::registerAndActivate([
            'email'    => 'admin@admin.com',
            'password' => 'admin',
            'first_name' => 'Admin',
            'last_name' => '',
        ]);

        Sentinel::registerAndActivate([
            'email'    => 'grabli1989@gmail.com',
            'password' => '3581321',
            'first_name' => 'Denis',
            'last_name' => '',
        ]);

        $this->command->info('Users seeded!');

    }
}
