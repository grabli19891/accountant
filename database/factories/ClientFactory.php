<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Client::class, function (Faker\Generator $faker) {

    $faker->addProvider(new Faker\Provider\he_IL\Person($faker));
    $faker->addProvider(new Faker\Provider\he_IL\PhoneNumber($faker));

    return [
        'user_id' => 3,
        'number' => $faker->randomNumber(),
        'name' => $faker->lastName,
        'passport' => $faker->randomNumber(),
        'tiknikuim' => $faker->randomNumber(),
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
    ];
});
