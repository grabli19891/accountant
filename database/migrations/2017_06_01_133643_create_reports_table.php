<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->primarykey();
            $table->string('name', 255);
            $table->string('settings')->nullable();
            $table->enum('status', ['pending', 'draft', 'complete']);
            $table->enum('type', [
                'annual-report',
                'declaration-of-capital',
                'ishur-nikui-mas-bamakor',
                'mam',
                'mas-ahnasa',
                'bituach-leumi',
                'itra',
                'tipulim-shonim',
                'custom'
            ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
