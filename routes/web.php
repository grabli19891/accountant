<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

# HomeController
Route::get('/', 'HomeController@index')->name('default');
Route::get('wait', "HomeController@wait")->name('wait');

Route::group(['prefix' => '/', 'middleware' => 'guest'], function () {
    # Registration
    Route::get('registration', 'Auth\RegistrationController@create')->name('registration.create');
    Route::post('registration', 'Auth\RegistrationController@store')->name('registration.store');
    Route::get('activate/{id}/{code}', 'Auth\ActivationController@activate')->name('activate');
    Route::get('reactivate/{id}','Auth\RegistrationController@reactivate')->name('reactivate')->where('id', '\d+');
# Login
    Route::get('login', 'Auth\SessionsController@create')->name('login');
    Route::post('login', 'Auth\SessionsController@store')->name('session.store');
# Reset
    Route::get('reset', 'Auth\ResetController@index')->name('reset.index');
    Route::post('reset', 'Auth\ResetController@stepOne')->name('reset.step.one');
    Route::get('reset/{id}/{code}', 'Auth\ResetController@stepTwo')->name('reset.step.two');
    Route::post('reset/{id}/{code}', 'Auth\ResetController@stepFinish')->name('reset.step.finish');
});
# Logout
Route::get('logout', 'Auth\SessionsController@destroy')->name('logout');

# Admin panel
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/', 'Admin\AdminPanel@index')->name('admin.index');

    Route::resource('users', 'Admin\UsersController');
    Route::resource('roles', 'Admin\RolesController');
});

# User controller
Route::put('update_profile', 'UserController@updateProfile')->name('update-profile');

# Dashboard
Route::group(['prefix' => 'dashboard', 'middleware' => 'logged_in'], function () {
    Route::get('/', 'Dashboard\DashboardController@index')->name('dashboard.index');
    Route::get('settings/{tab?}', 'Dashboard\DashboardController@settings')->name('dashboard.settings');
    Route::get('customReport/{id}/destroy', 'Dashboard\DashboardController@customReportDelete')->name('custom-report.delete')->where('id', '\d+');

    Route::group(['prefix' => '/'], function () {
        Route::resource('clients', 'Dashboard\ClientsController');
        // Attach report to client
        Route::post('clients/{id}/attachReport', 'Dashboard\ClientsController@attachReport')
            ->where('id', '\d+')
            ->name('clients.attachReport');
        Route::post('clients/{id}/reportComplete', 'Dashboard\ClientsController@reportComplete')
            ->where('id', '\d+')
            ->name('clients.reportComplete');
//        Route::get('clients/get-client', 'Dashboard\ClientsController@getClient')->name('clients.get-client');
    });

    Route::group(['prefix' => '/'], function () {
        Route::resource('reports', 'Dashboard\ReportsController');
        Route::post('reports/createCustomReport', 'Dashboard\ReportsController@createCustomReport')->name('reports.createCustomReport');
    });

    Route::group(['prefix' => '/'], function () {
        Route::resource('comments', 'Dashboard\CommentsController');
    });

    Route::group(['prefix' => '/'], function () {
        Route::resource('todos', 'Dashboard\TodoController');

        Route::get('todos/{id}/complete', 'Dashboard\TodoController@todoComplete')
            ->where('id', '\d+')->name('todos.todoComplete');
    });
});